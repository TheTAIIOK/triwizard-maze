/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import maze.Character;
import navigation.Direction;

/**
 *
 * @author godric
 */
public abstract class MovingAI implements StrategyAI {
    
    protected static Direction[] directions = {
        Direction.east(), Direction.north(),
        Direction.south(), Direction.west()
    };
    
    protected view.Game _game;
    protected Character _character;
    
    public MovingAI(view.Game game, Character obj) {
        _game = game;
        _character = obj;
    }
    
    @Override
    public void update(long l) {
        _character.move(getDirection(l));
    }
    
    protected abstract Direction getDirection(long elapsedTime);
}
