/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.Point;
import navigation.Direction;

/**
 *
 * @author godric
 */
public class AIMath {

    public static double degreesToRadians(double angle) {
        return angle * Math.PI / 180.0;
    }

    public static int radiansToDegrees(double angle) {
        return (int) (angle * 180.0 / Math.PI);
    }

    public static double distance(int x1, int y1, int x2, int y2) {
        return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public static double distance(Point p1, Point p2) {
        return AIMath.distance(p1.x, p1.y, p2.x, p2.y);
    }

    public static int angle(Point p1, Point p2) {
        int angle = radiansToDegrees(Math.atan((p2.y - p1.y) / (double) (p2.x - p1.x)));
        if (p2.x < p1.x) {
            angle += 180;
        }
        return angle;
    }

    public static int angle(int x1, int y1, int x2, int y2) {
        int angle = radiansToDegrees(Math.atan((y2 - y1) / (double) (x2 - x1)));
        if (x2 < x1) {
            angle += 180;
        }
        return angle;
    }

    public static Point getOppositePoint(Point p, Point center) {
        int newX = p.x - 2 * (p.x - center.x);
        int newY = p.y - 2 * (p.y - center.y);
        return new Point(newX, newY);
    }

    public static Direction getDirectionOfAngle(int angle) {
        switch (angle) {
            case 0:
                return Direction.east();
            case 90:
                return Direction.north();
            case 180:
                return Direction.west();
            case 270:
                return Direction.south();
            default:
                return null;
        }
    }

}
