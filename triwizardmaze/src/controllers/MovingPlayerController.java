/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import maze.Wizard;
import java.awt.event.KeyEvent;
import navigation.Direction;

/**
 *
 * @author godric
 */
public class MovingPlayerController extends MovingAI {
    
    public MovingPlayerController(view.Game game, Wizard hero) {
        super(game, hero);
    }
    
    @Override
    protected Direction getDirection(long elapsedTime) {
        Direction d = null;
        if( !_game.getTeleport())
        {
            if (_game.keyPressed(KeyEvent.VK_UP) || _game.keyPressed(KeyEvent.VK_W)) {   
                d = Direction.north();
            } else if (_game.keyPressed(KeyEvent.VK_DOWN) || _game.keyPressed(KeyEvent.VK_S)) {
                d = Direction.south();
            } else if (_game.keyPressed(KeyEvent.VK_LEFT) || _game.keyPressed(KeyEvent.VK_A)) {
                d = Direction.west();
            } else if (_game.keyPressed(KeyEvent.VK_RIGHT) || _game.keyPressed(KeyEvent.VK_D)) {
                d = Direction.east();
            }
        }
        return d;
    }
    
}
