/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import magic.Perricullum;
import magic.Diffindo;
import magic.Protego;
import magic.Ferula;
import maze.Wizard;
import java.awt.event.KeyEvent;
import magic.Expelliarmus;
import view.Game;
import view.gui.GGameScreen;

/**
 *
 * @author godric
 */
public class CastingPlayerAI extends CastingAI {

    public CastingPlayerAI(Game _game, Wizard _wizard) {
        super(_game, _wizard);
    }

    @Override
    protected Class getSpell(long l) {
        if( !_game.getTeleport()){
            if (_game.keyPressed(KeyEvent.VK_1)) {   
                return Diffindo.class;
            } else if (_game.keyPressed(KeyEvent.VK_4)) {
                return Ferula.class;
            } else if (_game.keyPressed(KeyEvent.VK_2)) {
                return Protego.class;
                /* } else if (_game.keyPressed(KeyEvent.VK_Z)) {
                return Perricullum.class;*/
            } else if (_game.keyPressed(KeyEvent.VK_3)) {
                return Expelliarmus.class;
            } else if (_game.keyPressed(KeyEvent.VK_5)){
                _game.setTeleport(true);
                ((GGameScreen)_game.getFrame().getContentPane()).setSelectorState(true);
            }
        } else if(_game.keyPressed(KeyEvent.VK_5)){
                _game.setTeleport(false);
                ((GGameScreen)_game.getFrame().getContentPane()).setSelectorState(false);
            }
        return null;
    }
    
    
    
}
