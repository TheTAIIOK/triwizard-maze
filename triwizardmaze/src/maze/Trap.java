/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import navigation.CellPosition;

/**
 *
 * @author godric
 */
public class Trap extends AbstrObject<CellPosition> {
    
    private int _minusHealth;
    
    public Trap() {
        super(null);
        _minusHealth = 0;
    }
    
    public Trap(World world, int minus) {
        super(world);
        this._minusHealth = minus;
    }

    public int getMinusHealth() {
        return _minusHealth;
    }

    @Override
    public String toString() {
        return super.toString() + ' ' + 
               _minusHealth;
    }

    //--------------------------------------------------------------------------
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        super.readExternal(in); //To change body of generated methods, choose Tools | Templates
        _minusHealth = in.readInt();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal(out); //To change body of generated methods, choose Tools | Templates.
        out.writeInt(_minusHealth);
    }

    
}
