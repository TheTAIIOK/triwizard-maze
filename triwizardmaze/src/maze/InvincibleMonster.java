/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import magic.AttackingMagic;
import navigation.CellPosition;
import navigation.Direction;
import navigation.MiddlePosition;

/**
 *
 * @author thane
 */
public class InvincibleMonster extends Enemy{
    
    public InvincibleMonster() {
        super(null);
    }
    
    public InvincibleMonster(World world) {
        super(world);
    }

        
    @Override
    public void hadBeenHit(AttackingMagic spell){
        _world.removeObject(spell);
    }
    
    @Override
    public void injure(int m){
    }
    
    @Override
    public void attack(Wizard w){
        if(!w.isProtected())
            w.injure(10);
    }
    
    @Override
    public void die(){
    }
    
    @Override
    public boolean canMove(Direction d){
        
         CellPosition test = new CellPosition(_position.calcNewPosition(_position.row(), _position.column(), d)[0],
                _position.calcNewPosition(_position.row(), _position.column(), d)[1]);
        return _position.hasNext(d)
                && _world.objects(Wall.class, new MiddlePosition(_position, d)).isEmpty() && 
                _world.objects(InvincibleMonster.class, test).isEmpty() && 
                _world.objects(SimpleMonster.class, test).isEmpty();
    }
        
}
