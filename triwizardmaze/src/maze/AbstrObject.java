/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import com.golden.gamedev.object.Sprite;
import java.awt.Point;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import navigation.CellPosition;
import navigation.MiddlePosition;

/**
 * Abstract class for all object in game.
 * @author godric
 */
public abstract class AbstrObject<Position> extends Sprite implements Externalizable {

    protected World _world;
    protected Position _position;
    
    public AbstrObject(World world) {
        _world = world;
        _position = null;
        
    }

    public Position position() {
        return _position;
    }

    public void setWorld(World _world) {
        if (_world != null) {
            this._world = _world;
        }
    }

    /**
     * set position of object
     *
     * @param pos
     * @return
     */
    public boolean setPosition(Position pos) {
        if (pos != null) {
            _position = pos;
            Point p;
            CellPosition pc;
            if (pos.getClass() == MiddlePosition.class) {
                pc = ((MiddlePosition) pos).cellPosition();
            } else {
                pc = (CellPosition) pos;
            }
            p = view.Game.CellToPoint(pc);
            this.setLocation(p.getX(), p.getY());
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return _position.toString();
    }

    //--------------------------------------------------------------------------
    // Override method to save/load object.
    
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(_position);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        _position = (Position) in.readObject();
    }

}
