/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import game.GameSetting;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Timer;
import java.util.TimerTask;
import navigation.CellPosition;
import navigation.Direction;
import navigation.MiddlePosition;
import magic.Diffindo;
import magic.Expelliarmus;
import magic.Ferula;
import magic.Magic;
import magic.Perricullum;
import magic.Protego;
import view.gui.IWizardListener;
import controllers.Stoppable;
import java.awt.image.BufferedImage;

/**
 *
 * @author godric
 */
public class Wizard extends Character implements Stoppable {

    private boolean _disarmed;
    private Timer _startMagic;
    
    GameSetting.Heroes _hero;

    private boolean _giveUp;

    IWizardListener _listener;
    
    BufferedImage[] _directions;
    
    public Wizard() {
        super(null);
        _hero = GameSetting.Heroes.HARRY;
        _giveUp = false;
        _disarmed = false;
        this._startMagic = new Timer();
    }

    public Wizard(GameSetting.Heroes hr, World w) {
        super(w);
        this._hero = hr;
        _giveUp = false;
        _disarmed = false;
        this._startMagic = new Timer();
    }
    
    @Override
    public void move(Direction d) {
        super.move(d);
        
        if (getDirection().equals(Direction.south())){
            setImage(_directions[0]);
        } else if (getDirection().equals(Direction.west())){
            setImage(_directions[1]);
        } else if (getDirection().equals(Direction.east())){
            setImage(_directions[2]);
        } else {
            setImage(_directions[3]);
        }
    }
    
    public void setDirectionImages(BufferedImage[] directions){
        _directions = directions;
    }
    
    public void setDisarmed(boolean value){
        this._disarmed = value;
        if(value){
            this._startMagic.schedule(new TimerTask() {
            @Override
            public void run() {
                _disarmed = false;
            }
        }, 5000);
        }
    }
    
    public boolean getDisarmed(){
        return _disarmed;
    }
    

    // Methods for health.
    @Override
    public void injure(int m) {
        super.injure(m);
        if (_listener != null)
        _listener.healthChanged(_health);
    }
    
    @Override
    public void cure(int m) {
        super.cure(m);
        if (_listener != null)
        _listener.healthChanged(_health);
    }
    
    @Override
    public void setHealth(int _health) {
        super.setHealth(_health);
        if (_listener != null)
        _listener.healthChanged(this._health);
    }
    
    // Methods for mana.
    @Override
    public void setMana(int _mana) {
        super.setMana(_mana);
        if (_listener != null)
        _listener.manaChanged(_mana);
    }
    
    @Override
    public void recoverMana(int value) {
        super.recoverMana(value);
        if (_listener != null)
        _listener.manaChanged(_mana);
        
    }
    
    public void setGiveUp(boolean d) {
        _giveUp = d;
    }

    public boolean getGiveUp() {
        return _giveUp;
    }

    public GameSetting.Heroes getName() {
        return this._hero;
    }

    public Magic cast(Class spell, CellPosition startpos, Direction direct) {
        Magic res = null;
        if (spell == Diffindo.class) {
            res = new Diffindo(_world, direct, this, 30);
        } else if (spell == Ferula.class) {
            res = new Ferula(_world, this);
        } else if (spell == Protego.class) {
            res = new Protego(_world, this);
        } else if (spell == Perricullum.class) {
            res = new Perricullum(_world, direct, this);
        } else if (spell == Expelliarmus.class) {
            res = new Expelliarmus(_world, direct, this);
        }
        
        return res;
    }

    public void makeSpell(Class spell) {
        Magic mg = cast(spell, this._position, this._direction);
        if (mg.castASpell()) {
            _world.addObject(this._position, mg);
        }
    }

    @Override
    public String toString() {
        return super.toString() + ' ' + _giveUp + ' '
                + _hero + ' ' + _disarmed;
    }

    @Override
    public boolean canMove(Direction d) {
        CellPosition test = new CellPosition(_position.calcNewPosition(_position.row(), _position.column(), d)[0],
                _position.calcNewPosition(_position.row(), _position.column(), d)[1]);

        return _position.hasNext(d)
                && _world.objects(Wall.class, new MiddlePosition(_position, d)).isEmpty() && _world.objects(Wizard.class, test).isEmpty() 
                && _world.objects(InvincibleMonster.class, test).isEmpty() && _world.objects(SimpleMonster.class, test).isEmpty() ;

    }

    //--------------------------------------------------------------------------
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal(out); //To change body of generated methods, choose Tools | Templates.
        out.writeBoolean(_giveUp);
        out.writeInt(_hero.value);
        out.writeBoolean(_disarmed);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        super.readExternal(in); //To change body of generated methods, choose Tools | Templates.
        _giveUp = in.readBoolean();
        _hero = GameSetting.Heroes.valueOf(in.readInt());
        _disarmed = in.readBoolean();
    }

    public void assignListener(IWizardListener listener) {
        _listener = listener;
    }

    private int manaBar;
    
    @Override
    public void pause(){
        manaBar = this._mana;
        _paused = true;
    }
    
    @Override
    public void resume(){
        
        this._mana = manaBar;
        _paused = false;       
    }
    
public boolean isPlayer(){
        return _listener != null;
    }

    public void teleport(int x, int y) {
        if (_mana == 100){
            int yCoord = (x-120)/39 +1;
            int xCoord = (y-50)/39 +1;
            CellPosition cell = new CellPosition(xCoord, yCoord);
            if(_world.objects(Wizard.class, cell).isEmpty()){
                this.setPosition(cell);
                this.setMana(0);
            }
        }
        else{
            System.out.print("You shall not pass! \n");
        }
    }
}
