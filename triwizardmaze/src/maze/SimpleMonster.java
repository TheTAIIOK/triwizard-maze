/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import magic.AttackingMagic;
import navigation.CellPosition;
import navigation.Direction;
import navigation.MiddlePosition;

/**
 *
 * @author thane
 */
public class SimpleMonster  extends Enemy{
    
    public SimpleMonster() {
        super(null);
    }
    
    public SimpleMonster(World world) {
        super(world);
    }
    
    @Override
    public void hadBeenHit(AttackingMagic spell){
        this.injure(spell.getDamage());
        spell.setActive(false);
    }
    
    @Override
    public void attack(Wizard w){
        if(!w.isProtected())
            w.injure(1);
    }

    @Override
    public void die() {
        _world.removeObject(this);
    }
    
     @Override
    public boolean canMove(Direction d){
        
         CellPosition test = new CellPosition(_position.calcNewPosition(_position.row(), _position.column(), d)[0],
                _position.calcNewPosition(_position.row(), _position.column(), d)[1]);
        return _position.hasNext(d)
                && _world.objects(Wall.class, new MiddlePosition(_position, d)).isEmpty() && 
                _world.objects(InvincibleMonster.class, test).isEmpty() && 
                _world.objects(SimpleMonster.class, test).isEmpty();
    }
}
