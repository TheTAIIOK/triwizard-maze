/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maze;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 *
 * @author godric
 */
public class Goblet extends Item {



    public Goblet() {
        super(null);
    }

    public Goblet(World world) {
        super(world);
    }

    @Override
    public String toString() {
        return _position.toString();
    }

    //--------------------------------------------------------------------------
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        super.readExternal(in);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        super.writeExternal(out);
    }

}
