/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collisions;

import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.collision.CollisionBounds;

/**
 *
 * @author godric
 */
public class MagicOutCollision extends CollisionBounds {

    public MagicOutCollision(int i, int i1, int i2, int i3) {
        super(i, i1, i2, i3);
    }

    @Override
    public void collided(Sprite sprite) {
        sprite.setActive(false);
    }
    
}
