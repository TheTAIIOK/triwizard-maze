/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collisions;

import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.collision.AdvanceCollisionGroup;
import game.Triwizardmaze;
import maze.FakeGoblet;

/**
 *
 * @author thane
 */
public class WizardFoundFakeGoblet extends AdvanceCollisionGroup {

    public WizardFoundFakeGoblet() {
        pixelPerfectCollision = true;
    }

    @Override
    public void collided(Sprite wizard, Sprite fakeGoblet) {
        if (((maze.Wizard) wizard).isPlayer()) {
            Triwizardmaze.getPlayerInstance().lastHighscore += 100;
        }

        ((FakeGoblet) fakeGoblet).take((maze.Character) wizard);
    }
}

