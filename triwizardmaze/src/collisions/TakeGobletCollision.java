/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collisions;

import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.collision.AdvanceCollisionGroup;
import game.Triwizardmaze;
import maze.Goblet;

/**
 * 
 * @author godric
 */
public class TakeGobletCollision extends AdvanceCollisionGroup {

    public TakeGobletCollision() {
        pixelPerfectCollision = true;
    }
    
    @Override
    public void collided(Sprite wizard, Sprite goblet) {
        if (((maze.Wizard) wizard).isPlayer()) {
            Triwizardmaze.getPlayerInstance().lastHighscore += 1000;
        }
        
        ((Goblet)goblet).take((maze.Character)wizard);
    }
    
}