/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collisions;

import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.collision.AdvanceCollisionGroup;
import maze.Wizard;

/**
 *
 * @author thane
 */
public class MonsterHitWizardCollision extends AdvanceCollisionGroup{
    public MonsterHitWizardCollision() {
        pixelPerfectCollision = true;
    }

    @Override
    public void collided(Sprite monster, Sprite wizard) {
        maze.Enemy e = (maze.Enemy) monster;
        System.out.print("ITS ALIVEEE");
        e.attack((Wizard)wizard);
    }
}
