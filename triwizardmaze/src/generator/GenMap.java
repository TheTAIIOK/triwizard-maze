/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generator;

import game.GameSetting;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import maze.Wall;
import navigation.CellPosition;
import navigation.Direction;
import navigation.MiddlePosition;

/**
 *
 * @author godric
 */
public class GenMap {

    /**
     * XXXXXXXXX 
     * Y   0 
     * Y 3 x 1 
     * Y   2
     */
    
    private final int width;
    private final int height;
    
    private final boolean[][] visited;
    private final boolean[][][] paths;
    private final int[] dx;
    private final int[] dy;
    
    private final Random randomer;
    
    public GenMap() {
        this.dy = new int[] {-1, 0, 1, 0};
        this.dx = new int[] {0, 1, 0, -1};
        
        this.height = GameSetting.HEIGHT;
        this.width = GameSetting.WIDTH;
         
        this.visited = new boolean[width][height];
        this.paths = new boolean[width][height][4];
        
        CellPosition.setHorizontalRange(1, width);
        CellPosition.setVerticalRange(1, height);
        
        randomer = new Random();
        
    }
    
    private void inits() {
        
        for (boolean[] v : visited) {
            Arrays.fill(v, false);
        }
        
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                Arrays.fill(paths[i][j], false);
            }
        }
        
    }
    
    private boolean checkPos(int i, int j) {
        if (0 <= i && i < width
                && 0 <= j && j < height) {
            return !visited[i][j];
        }
        return false;
    }
    
    private ArrayList<Integer> getPossible(int i, int j) {
        
        ArrayList<Integer> res;
        res = new ArrayList<>();
        for (int k = 0; k < 4; k++) {
            if (checkPos(i + dx[k], j + dy[k])) {
                res.add(k);
                res.add(i + dx[k]);
                res.add(j + dy[k]);
            }
        }
        return res;
    }
    
    
    private void dfs(int i, int j, int d) {

        ArrayList<Integer> nexts;
        visited[i][j] = true;
        nexts = getPossible(i, j);
        int k = 0;
        int bound = 0;
        
        while (!nexts.isEmpty()) {
            
            bound = nexts.size() / 3;
            k = bound == 0 ? 0 : randomer.nextInt(bound);
            
            // dont change direction
            if (randomer.nextInt(100) < -1) {
                for (int old = 0; old < nexts.size()/3; old++) {
                    if (nexts.get(old*3) == d) {
                        k = old;
                        break;
                    }
                }
            }
//            System.out.printf("%d %d --> %d %d\n", j+1, i+1, nexts.get(k*3+2)+1, nexts.get(k*3+1)+1);
            paths[i][j][nexts.get(k*3)] = true;
            paths[nexts.get(k*3 + 1)][nexts.get(k*3 + 2)][(nexts.get(k*3)+2)%4] = true;
            dfs(nexts.get(k*3 + 1), nexts.get(k*3 + 2), nexts.get(k*3));
            nexts = getPossible(i, j);
            
        }
        
    }
    
    private ArrayList<Wall> genWalls() {
        
        ArrayList<Wall> res = new ArrayList<>();
        Wall tmp = null;
        
        inits();
        switch (randomer.nextInt(4)) {
            case 0:
                dfs(0, 0, randomer.nextInt(2)+1);
                break;
            case 1:
                dfs(width-1, 0, randomer.nextInt(2)+2);
                break;
            case 2:
                dfs(0, height-1, randomer.nextInt(2));
                break;
            case 3:
                dfs(width-1, height-1, (randomer.nextInt(2)+3)%4);
                break;
        }
        int ii, jj;
        
        
//        for (int i = 0; i < width; i++) {
//            for (int j = 0; j < height; j++) {
//                System.out.printf("%d %d ", i, j);
//                for (int k = 0; k < 4; k++) {
//                    System.out.printf("%s ", paths[i][j][k]);
//                }
//                System.out.println("");
//            }
//        }
        
        
        for (int j = 0; j < height; ++j) {
            for (int i = 0; i < width; i++) {
                ii = i + dx[0]; jj = j + dy[0];
                
                if (0 <= ii && ii < width && 0 <= jj && jj < height) {
//                    System.out.printf("%d%d %d%d %s %s\n", j+1, i+1, jj+1, ii+1, paths[i][j][2], paths[ii][jj][0]);
                    if(!paths[i][j][0] && !paths[ii][jj][2]) {
                        tmp = new Wall();
                        tmp.setPosition(new MiddlePosition(new CellPosition(j+1, i+1), Direction.north()));
                        res.add(tmp);
//                        System.out.printf(" * ");
                    }
//                    System.out.println("");
                }
                //------------------------------------------------------------------------------
                ii = i + dx[3]; jj = j + dy[3];
                if (0 <= ii && ii < width && 0 <= jj && jj < height) {
//                    System.out.printf("%d%d %d%d %s %s\n", j+1, i+1, jj+1, ii+1, paths[i][j][3], paths[ii][jj][1]);
                    if (!paths[i][j][3] && !paths[ii][jj][1]) {
                        tmp = new Wall();
                        tmp.setPosition(new MiddlePosition(new CellPosition(j+1, i+1), Direction.west()));
                        res.add(tmp);
//                        System.out.printf(" * ");
                    }
//                    System.out.println("");
                }
            }
        }
        
        return res;

    }
    
    
    //--------------------------------------------------------------------------
    private static GenMap _instance = new GenMap();
    public static ArrayList<Wall> run() {
        
        return _instance.genWalls();
        
    }
    
    //--------------------------------------------------------------------------
    public int MAX = 500;
    public static void main(String[] args) {
        
        GenMap g = new GenMap();
        ArrayList<Wall> all = new ArrayList<>();
        ArrayList<Wall> tmp;
        
        try (ObjectOutputStream oo = new ObjectOutputStream(new FileOutputStream("labrs.cer"))) {

            oo.writeInt(g.MAX);
            for (int i = 0; i < g.MAX; i++) {
                tmp = g.run();
                oo.writeInt(tmp.size());
                for (Wall wall : tmp) {
                    oo.writeObject(wall);
                }
                all.addAll(tmp);
            }
            

        } catch (IOException ex) {
            System.out.println("Exception");
            
        }
        System.out.println("Save done!");
        
    }

}
