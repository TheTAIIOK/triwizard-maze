/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package navigation;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 *
 * @author godric
 */
public class Direction implements Externalizable {

    // direction - angle [0..360] degree
    private int _angle = 90;

    private Direction(int angle) {
        angle = angle % 360;
        if (angle < 0) {
            angle += 360;
        }
        this._angle = angle;
    }

    // ------------------ Available directions ---------------------
    public static Direction north() {
        return new Direction(90);
    }

    public static Direction south() {
        return new Direction(270);
    }

    public static Direction east() {
        return new Direction(0);
    }

    public static Direction west() {
        return new Direction(180);
    }

    // ------------------ Change direction ---------------------
    @Override
    public Direction clone() {
        return new Direction(this._angle);
    }

    public Direction clockwise() {
        return new Direction(this._angle - 90);
    }

    public Direction anticlockwise() {
        return new Direction(this._angle + 90);
    }

    public Direction opposite() {
        return new Direction(this._angle + 180);
    }

    public Direction rightword() {
        return clockwise();
    }

    public Direction leftword() {
        return anticlockwise();
    }

    public int clockwise_angle(Direction other) {
        int res = this._angle - other._angle;
        if (res < 0) {
            res += 360;
        }
        return res;
    }

    // ------------------ Compare ---------------------
    @Override
    public boolean equals(Object other) {

        if (other instanceof Direction) {
            // “ипы совместимы, можно провести преобразование
            Direction otherDirect = (Direction) other;
            // ¬озвращаем результат сравнени¤ углов
            return _angle == otherDirect._angle;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return this._angle;
    }

    public boolean isOpposite(Direction other) {
        return this.opposite().equals(other);
    }

    @Override
    public String toString() {
        return String.valueOf(_angle);
    }

    public int get_angle() {
        return this._angle;
    }

    //--------------------------------------------------------------------------
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(_angle);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this._angle = in.readInt();
    }
}
