/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package navigation;

import controllers.AIMath;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Position between 2 cells. Direction in north or west if position not in right
 * or bottom border
 *
 * @author godric
 */
public class MiddlePosition implements Externalizable {

    private CellPosition _cellPosition;
    private Direction _direction;

    // ---------------------------------------
    public MiddlePosition() {
        _cellPosition = null;
        _direction = null;
    }

    public MiddlePosition(CellPosition cellPos, Direction direct) {

        if (!cellPos.isValid()) {
            // ТODO Error here
        }

        _cellPosition = cellPos;
        _direction = direct;

        normalize();
    }

    private void normalize() {

        // Turn to North if can
        if (_direction.equals(Direction.south()) && _cellPosition.hasNext(_direction)) {
            _cellPosition = _cellPosition.next(_direction);
            _direction = Direction.north();
        }

        // turn to West if can
        if (_direction.equals(Direction.east()) && _cellPosition.hasNext(_direction)) {
            _cellPosition = _cellPosition.next(_direction);
            _direction = Direction.west();
        }
    }

    public Direction direction() {
        return _direction;
    }

    public CellPosition cellPosition() {
        return _cellPosition;
    }

    @Override
    public MiddlePosition clone() {
        return new MiddlePosition(_cellPosition, _direction);
    }

    // ------------------ Сравнение позиций ---------------------
    public boolean equals(Object other) {

        if (other instanceof MiddlePosition) {
            // same types
            MiddlePosition otherPosition = (MiddlePosition) other;
            return _cellPosition.equals(otherPosition._cellPosition)
                    && _direction.equals(otherPosition._direction);
        }

        return false;
    }

    @Override
    public String toString() {
        return _cellPosition.toString() + ' ' + _direction.toString();
    }

    //--------------------------------------------------------------------------
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(_cellPosition);
        out.writeInt(_direction.get_angle());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        _cellPosition = (CellPosition) in.readObject();
        _direction = AIMath.getDirectionOfAngle(in.readInt());
    }
}
