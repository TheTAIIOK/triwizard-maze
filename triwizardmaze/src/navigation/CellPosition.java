/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package navigation;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.HashMap;

/**
 *
 * @author godric
 */
public class CellPosition implements Externalizable {

    // ------------------ Cell range --------------------------------
    private static PosRange _horizontalRange = new PosRange(0, 0);
    private static PosRange _verticalRange = new PosRange(0, 0);

    public static void setHorizontalRange(int min, int max) {
        if (PosRange.isValidRange(min, max)) {
            _horizontalRange = new PosRange(min, max);
        }
    }

    public static PosRange horizontalRange() {
        return _horizontalRange;
    }

    public static void setVerticalRange(int min, int max) {
        if (PosRange.isValidRange(min, max)) {
            _verticalRange = new PosRange(min, max);
        }
    }

    public static PosRange verticalRange() {
        return _verticalRange;
    }
    //---------------------------------------------------------------

    private int row;
    private int column;

    public CellPosition() {
        row = -1;
        column = -1;
    }

    public CellPosition(int row, int col) {
        if (!isValid(row, col)) {  //  TODO Error here
        }

        this.row = row;
        this.column = col;
    }

    public int row() {

        if (!isValid()) {  //  TODO Error here
        }

        return row;
    }

    public int column() {

        if (!isValid()) {  //  TODO Error here
        }

        return column;
    }

    // --------------- checking ----------------------
    public boolean isValid() {
        return isValid(row, column);
    }

    public static boolean isValid(int row, int col) {
        return _horizontalRange.contains(col) && _verticalRange.contains(row);
    }

    @Override
    public CellPosition clone() {
        return new CellPosition(row, column);
    }

    // ------------------ Порождение и проверка смежных позиций ---------------------
    public CellPosition next(Direction direct) {

        int[] newPos = calcNewPosition(row, column, direct);
        return new CellPosition(newPos[0], newPos[1]);
    }

    public boolean hasNext(Direction direct) {

        int[] newPos = calcNewPosition(row, column, direct);
        return isValid(newPos[0], newPos[1]);
    }

    // @return int[] row, col of new position with direction
    public int[] calcNewPosition(int row, int col, Direction direct) {

        // Direction to coordinate
        HashMap<Direction, int[]> offset = new HashMap<Direction, int[]>();
        offset.put(Direction.north(), new int[]{0, -1});
        offset.put(Direction.south(), new int[]{0, 1});
        offset.put(Direction.east(), new int[]{1, 0});
        offset.put(Direction.west(), new int[]{-1, 0});

        int[] newPos = new int[2];

        newPos[0] = row + offset.get(direct)[1];
        newPos[1] = column + offset.get(direct)[0];

        return newPos;
    }

    // ------------------ Сравнение позиций ---------------------
    @Override
    public boolean equals(Object other) {

        if (!isValid()) {  //  TODO Error here
        }

        if (other instanceof CellPosition) {
            // objects -- same type
            CellPosition otherPosition = (CellPosition) other;
            return row == otherPosition.row && column == otherPosition.column;
        }

        return false;
    }

    @Override
    public String toString() {
        return row + " " + column;
    }

    //--------------------------------------------------------------------------
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(row);
        out.writeInt(column);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        row = in.readInt();
        column = in.readInt();
    }
}
