/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.engine.BaseLoader;
import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.TLabel;
import com.golden.gamedev.gui.toolkit.TContainer;
import game.Triwizardmaze;
import java.awt.image.BufferedImage;
import maze.Maze.EndReason;

/**
 *
 * @author still
 */
public class GEndGameScreen extends TContainer {

    private IBasicSceneListener _modelListener;

    private TButton _restartButton;

    private TButton _backMenuButton;

    
    private TLabel _header;
    
    private TLabel _winHeader;
    
    private TLabel _deadHeader;
    
    private TLabel _surrHeader;
    
    private BaseLoader _loader;

    public GEndGameScreen(IBasicSceneListener model, BaseLoader io) {

        super(0, 0,
                (int) Triwizardmaze.WINDOW_SIZE.getWidth(),
                (int) Triwizardmaze.WINDOW_SIZE.getHeight());

        _modelListener = model;
        _loader = io;
        init();
    }

    @Override
    public String UIName() {
        return "Faded Screen";
    }

    public void init() {
        initButtons();
        
        BufferedImage headerRaw = _loader.getStoredImage(Header);
        
        _header = new TLabel ("", 140,50,headerRaw.getWidth(), headerRaw.getHeight());
        _header.setExternalUI(new BufferedImage[]{headerRaw, headerRaw}, false);
        this.add(_header);
        
        
    }

    private void initButtons() {
        _backMenuButton = new TButton("Back to menu", 600, 600, 250, 50){
            @Override
            public void doAction(){
                _modelListener.OnButtonEvent(_backMenuButton);
            }
        };
        this.add(_backMenuButton);
    }
    
    public void setup(EndReason endGameReason){
        BufferedImage[] messages = {
            _loader.getStoredImage(MsgWin),
            _loader.getStoredImage(MsgDie),
            _loader.getStoredImage(MsgGup)
        };
        
        BufferedImage[] picxs = {
            _loader.getStoredImage(EndAngel),
            _loader.getStoredImage(CedricPic),
            _loader.getStoredImage(PotterPic),
            _loader.getStoredImage(FleurPic),
            _loader.getStoredImage(KrumPic)
        };
        
        BufferedImage[] names = {
            _loader.getStoredImage(CedricLabel),
            _loader.getStoredImage(PotterLabel),
            _loader.getStoredImage(FleurLabel),
            _loader.getStoredImage(KrumLabel)
        };
        
        TLabel message = null;
        TLabel picx = null;
        TLabel name = null;
        
        switch (endGameReason){
            case PLAYER_DEAD:
                message = new TLabel("", 580,360, messages[1].getWidth(), messages[1].getHeight());
                message.setExternalUI(new BufferedImage[]{messages[1], messages[1]}, false);
                picx = new TLabel("", 100,280, picxs[0].getWidth(), picxs[0].getHeight());
                picx.setExternalUI(new BufferedImage[]{picxs[0], picxs[0]}, false);
                break;
                
            case PLAYER_SURREND:
                message = new TLabel("m", 600,300, messages[2].getWidth(), messages[2].getHeight());
                message.setExternalUI(new BufferedImage[]{messages[2], messages[2]}, false);

                picx = new TLabel("", 100,280, picxs[0].getWidth(), picxs[0].getHeight());
                picx.setExternalUI(new BufferedImage[]{picxs[0], picxs[0]}, false);
                break;
                
            case POTTER_WIN:
                message = new TLabel("m", 580,200, messages[0].getWidth(), messages[0].getHeight());
                message.setExternalUI(new BufferedImage[]{messages[0], messages[0]}, false);
                
                name = new TLabel("", 590,320, names[1].getWidth(), names[1].getHeight());
                name.setExternalUI(new BufferedImage[]{names[1], names[1]}, false);
                
                picx = new TLabel("", 120,300, picxs[2].getWidth(), picxs[2].getHeight());
                picx.setExternalUI(new BufferedImage[]{picxs[2], picxs[2]}, false);
                break;
                
            case KRUM_WIN:
                message = new TLabel("m", 580,200, messages[0].getWidth(), messages[0].getHeight());
                message.setExternalUI(new BufferedImage[]{messages[0], messages[0]}, false);
                
                name = new TLabel("", 590,320, names[3].getWidth(), names[3].getHeight());
                name.setExternalUI(new BufferedImage[]{names[3], names[3]}, false);
                
                picx = new TLabel("", 120,300, picxs[4].getWidth(), picxs[4].getHeight());
                picx.setExternalUI(new BufferedImage[]{picxs[4], picxs[4]}, false);
                break;
                
            case DIGGORY_WIN:
                message = new TLabel("m", 580,200, messages[0].getWidth(), messages[0].getHeight());
                message.setExternalUI(new BufferedImage[]{messages[0], messages[0]}, false);
                
                name = new TLabel("", 550,320, names[0].getWidth(), names[0].getHeight());
                name.setExternalUI(new BufferedImage[]{names[0], names[0]}, false);
                
                picx = new TLabel("", 120,300, picxs[1].getWidth(), picxs[1].getHeight());
                picx.setExternalUI(new BufferedImage[]{picxs[1], picxs[1]}, false);
                break;
                
            case DELACOUR_WIN:
                message = new TLabel("m", 580,200, messages[0].getWidth(), messages[0].getHeight());
                message.setExternalUI(new BufferedImage[]{messages[0], messages[0]}, false);
                
                name = new TLabel("", 560,320, names[2].getWidth(), names[2].getHeight());
                name.setExternalUI(new BufferedImage[]{names[2], names[2]}, false);
                
                picx = new TLabel("", 120,300, picxs[3].getWidth(), picxs[3].getHeight());
                picx.setExternalUI(new BufferedImage[]{picxs[3], picxs[3]}, false);
                break;
                
            
            default:
                throw new AssertionError(endGameReason.name());
    }
        this.add(message);
        this.add(picx);
        if (name != null){
            this.add(name);
        }
    }

    private static final String EndAngel = "pix/endscreen/end_angel";
    private static final String Header = "pix/endscreen/header";
    private static final String MsgDie = "pix/endscreen/msg_die";
    private static final String MsgGup = "pix/endscreen/msg_gup";
    private static final String MsgWin = "pix/endscreen/msg_win";
    private static final String CedricPic = "pix/endscreen/win_cedric";
    private static final String CedricLabel = "pix/endscreen/win_cedric_label";
    private static final String PotterPic = "pix/endscreen/win_potter";
    private static final String PotterLabel = "pix/endscreen/win_potter_label";
    private static final String FleurPic = "pix/endscreen/win_fleur";
    private static final String FleurLabel = "pix/endscreen/win_fleur_label";
    private static final String KrumLabel = "pix/endscreen/win_krum_label";
    private static final String KrumPic = "pix/endscreen/win_krum";
}
