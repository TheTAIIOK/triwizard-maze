/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.gui.TButton;
import java.awt.image.BufferedImage;

/**
 *
 * @author Alexander Lyashenko
 */
public class TImageButton extends TButton {

    BufferedImage _button;

    public TImageButton(String name, BufferedImage sprite, int i, int i1, int i2, int i3) {
        super(name, i, i1, i2, i3);
        _button = sprite;
    }

    @Override
    public String UIName() {
        return "Image Button";
    }
    
    public BufferedImage getButtonImage(){
        return _button;
    }

}
