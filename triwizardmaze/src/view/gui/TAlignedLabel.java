/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.gui.TLabel;

/**
 *
 * @author still
 */
public class TAlignedLabel extends TLabel {

    private String _fabricName;
    
    public TAlignedLabel(String string, int x, int y, int w, int h) {
        super(string, x, y, w, h);
    }

    public void alignLeft(){
        _fabricName = "LA Label";
    }
    
    public void alignRight(){
        _fabricName = "RA Label";
    }
    
    public void alignCenter(){
        _fabricName = "CA Label";
    }
    
    @Override
    public String UIName() {
        return _fabricName;
    }
}
