/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.gui.TLabel;
import com.golden.gamedev.object.Timer;

/** Label, that can appear or disappear on screen by timer
 * By default, label disabled after creating
 * @author still
 */
public class TTimedLabel extends TLabel{
    
    private Timer _messageTimer;
    
    public TTimedLabel(String text, int x, int y, int w, int h) {
        super(text, x, y, w, h);
        this.setEnabled(false);
    }
    
    @Override
    public String UIName() {
        return "Label";
    }
    
    @Override
    public void update(){
        super.update();
        
        if (_messageTimer != null && _messageTimer.action(1)){
            this.setEnabled(false);
            _messageTimer = null;
        }
    }
    
    public void showMessage (String message){
        this.setText(message);
        this.setEnabled(true);
        _messageTimer = new Timer(100);
    }
}
