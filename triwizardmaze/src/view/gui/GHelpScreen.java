/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view.gui;

import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.TLabel;
import com.golden.gamedev.gui.toolkit.TContainer;
import game.Triwizardmaze;

/**
 *
 * @author Alexander Lyashenko
 */
public class GHelpScreen extends TContainer {

    private IBasicSceneListener _modelListener;

    private String[] _buttonsText = {"Use WASD for movement", 
        "Use 1 or red button to attack enemies", 
        "Use 2 or green button to defend yourseld for few seconds", 
        "Use 3 or yellow button to stun enemies", 
        "Use 4 or blue button to heal yourself enemies", 
        "Use 5 or purple button to teleport to cell", 
        "Avoid enemies and be careful - not all trophies are real"};

    
    public GHelpScreen(IBasicSceneListener model) {
        super(0, 0,
        (int) Triwizardmaze.WINDOW_SIZE.getWidth(),
        (int) Triwizardmaze.WINDOW_SIZE.getHeight());
        _modelListener = model;
        init();
    }

    public void init() {
        for (int i = 0; i < _buttonsText.length; ++i){
            this.add(new TLabel( _buttonsText[i],TOP_X,TOP_Y + i*80, BUTTON_WIDTH, BUTTON_HEIGHT));
        }
        
        this.add(new TButton("Back", 10, 10, 100, 35){
            @Override
            public void doAction(){
                _modelListener.OnButtonEvent(this);
            }
        });
    }


    private final static int TOP_X = 100;
    private final static int TOP_Y = 100;
    private final static int BUTTON_WIDTH = 860;
    private final static int BUTTON_HEIGHT = 45;
    
    
    @Override
    public String UIName() {
        return "Faded Screen";
    }
}
