/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.toolkit.TContainer;
import game.Triwizardmaze;

/**
 *
 * @author still
 */
public class GPauseScreen extends TContainer {

    private IBasicSceneListener _modelListener;

    private String[] _buttonsText = {"Back", "Save", "Give up", "Help", "Exit to menu"};

    private TTimedLabel _infoMessage;
    
    public GPauseScreen(IBasicSceneListener model) {
        super(0, 0,
        (int) Triwizardmaze.WINDOW_SIZE.getWidth(),
        (int) Triwizardmaze.WINDOW_SIZE.getHeight());
        _modelListener = model;
        init();
    }

    public void init() {
        initButtons();

        /*Init message label*/
        _infoMessage = new TTimedLabel("info", 10, 5, 300, 35);
        this.add(_infoMessage);
    }

    /**
     * Initializes buttons on maze pause scene
     */
    private void initButtons() {
        for (int i = 0; i < _buttonsText.length; ++i) {
            this.add(new TButton(
                    _buttonsText[i],
                    TOP_X, TOP_Y + 88 * i,
                    BUTTON_WIDTH, BUTTON_HEIGHT) {
                @Override
                public void doAction() {
                    _modelListener.OnButtonEvent(this);
                }
            });
        }
    }

    public void showInfoMessage(String msg) {
        _infoMessage.showMessage(msg);
    }

    private final static int TOP_X = 387;
    private final static int TOP_Y = 160;
    private final static int BUTTON_WIDTH = 250;
    private final static int BUTTON_HEIGHT = 45;
    
    
    @Override
    public String UIName() {
        return "Faded Screen";
    }
}
