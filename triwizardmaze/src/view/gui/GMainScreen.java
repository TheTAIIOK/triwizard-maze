/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view.gui;

import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.toolkit.TContainer;
import game.Triwizardmaze;

/**
 *
 * @author Alexander Lyashenko
 */
public class GMainScreen extends TContainer {

    private IBasicSceneListener _modelListener;

    private String[] _buttonsText = {"New Game", "Load", "Profile", "Help", "Exit to menu"};

    private TButton[] _buttons;
    
   // private TTimedLabel _infoMessage;
    
    public GMainScreen(IBasicSceneListener model) {
       
        super(0, 0,
                (int) Triwizardmaze.WINDOW_SIZE.getWidth(),
                (int) Triwizardmaze.WINDOW_SIZE.getHeight());
        _modelListener = model;
        init();
    }

    public void init() {
        initButtons();
    }

    /**
     * Initializes buttons on maze pause scene
     */
    private void initButtons() {
        _buttons = new TButton [_buttonsText.length];
        
        for (int i = 0; i < _buttonsText.length; ++i) {
            _buttons[i] = new TButton(
                    _buttonsText[i],
                    BUTTON_X, BUTTON_Y + BUTTON_SPACE * i,
                    BUTTON_WIDTH, BUTTON_HEIGHT) {
                @Override
                public void doAction() {
                    _modelListener.OnButtonEvent(this);
                }
            };
            this.add(_buttons[i]);
        }    
    }
    
    private final static int BUTTON_X = 640;
    private final static int BUTTON_Y = 330;
    private final static int BUTTON_WIDTH = 250;
    private final static int BUTTON_HEIGHT = 48;
    private final static int BUTTON_SPACE = BUTTON_HEIGHT + 30;
    
    @Override
    public String UIName() {
        return "Transparent Screen";
    }

}
