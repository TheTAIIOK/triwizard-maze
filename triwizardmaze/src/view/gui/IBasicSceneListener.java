/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.gui.TButton;

/**
 *
 * @author still
 */
public interface IBasicSceneListener {
    public abstract void OnButtonEvent(TButton initiator);
}
