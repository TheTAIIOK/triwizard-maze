/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

/**
 *
 * @author still
 */
public interface IWizardListener {
    /**
     * Emits when wizard health changing
     * @param newValue - new value of health points
     */
    public abstract void healthChanged(int newValue);
    
    /**
     * Emits when wizard mana changing
     * @param newValue - new value of mana points
     */
    public abstract void manaChanged(int newValue);
    
    /**
     * Emits when availability of any spell changed
     * @param newValue byte, where every 1 bit means available, and 0 is not
     */
    public abstract void spellAvailabilityChanged (byte newValue);
}
