/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import com.golden.gamedev.engine.BaseLoader;
import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.TLabel;
import com.golden.gamedev.gui.toolkit.TContainer;
import com.golden.gamedev.util.ImageUtil;
import game.Triwizardmaze;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import maze.Wizard;

/**
 *
 * @author still
 */
public class GGameScreen extends TContainer {

    private IMazeViewListener _modelListener;

    private BaseLoader _loader;

    private int _barWidth;
    private int _barHeight;
    private int _barValueWidth;
    private int _barValueHeight;

    private TLabel _hpBar;
    private TLabel _hpBarState;
    private int _hpBarValue;
    private TAnimatedComponent _hpDragon;
    private final Color healthColor = new Color(215, 77, 40);
    private final Color healthBgColor = new Color(42, 15, 8);

    private TLabel _mpBar;
    private TLabel _mpBarState;
    private int _mpBarValue;
    private TAnimatedComponent _mpDragon;
    private final Color manaColor = new Color(40, 94, 215);
    private final Color manaBgColor = new Color(8, 10, 42);

    private TSelectorButton[][] _selector;
    private boolean inSelectorMode;
    
    private TLabel _wandbarPix;
    private TImageButton[] _spells;

    public GGameScreen(IMazeViewListener model, Wizard hero, BaseLoader io) {
        super(0, 0,
                (int) Triwizardmaze.WINDOW_SIZE.getWidth(),
                (int) Triwizardmaze.WINDOW_SIZE.getHeight());
        // this.customRendering = true;
        _modelListener = model;
        _loader = io;
        hero.assignListener(new IWizardListener() {
            @Override
            public void healthChanged(int newValue) {
                _hpBarValue = newValue;
                BufferedImage picValue = drawBarValue(
                        _barValueWidth, _barValueHeight,
                        _hpBarValue, healthColor, healthBgColor);
                _hpBarState.setExternalUI(new BufferedImage[]{picValue, picValue}, false);
            }

            @Override
            public void manaChanged(int newValue) {
                _mpBarValue = newValue;
                BufferedImage picValue = drawBarValue(
                        _barValueWidth, _barValueHeight,
                        _mpBarValue, manaColor, manaBgColor);
                _mpBarState.setExternalUI(new BufferedImage[]{picValue, picValue}, false);
            }

            @Override
            public void spellAvailabilityChanged(byte newValue) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

        });
        _hpBarValue = hero.getHealth();
        _mpBarValue = hero.getMana();

        _selector = new TSelectorButton[game.GameSetting.WIDTH][game.GameSetting.HEIGHT];
        init();
    }

    public void init() {
        initButtons();
        initSidebars();
        initWandBar();

        int startX = 120;
        int startY = 50;
        int cellWidth = 39;
        int cellHeight = 39;
        for (int i = 0; i < game.GameSetting.WIDTH; ++i) {
            //_selector[i][] = new 
            for (int j = 0; j < game.GameSetting.HEIGHT; ++j) {
                //int index = i * game.GameSetting.WIDTH + j;
                _selector[i][j] = new TSelectorButton("",
                        startX + i * cellWidth, startY + j * cellHeight, cellWidth, cellHeight){
                            @Override
                            public void doAction(){
                                setSelectorState(false);
                                _modelListener.selectorClicked(this.getX(), this.getY());
                            }
                        };
                _selector[i][j].setEnabled(false);
                this.add(_selector[i][j]);
            }
        }
        inSelectorMode = false;
    }

    /**
     * Initializes buttons on maze scene
     */
    private void initButtons() {
        this.add(new TButton("Pause", TOP_X, TOP_Y,
                BUTTON_WIDTH - 30, BUTTON_HEIGHT + 15) {
            @Override
            public void doAction() {
                _modelListener.OnButtonEvent(this);
            }
        });
    }

    private void initSidebars() {
        /*Health-points bar initialization*/

 /*Bar texture*/
        BufferedImage hpBarRaw = _loader.getStoredImage(HP_BAR_PATH);

        /*'cause all bars equal in sizes*/
        _barHeight = hpBarRaw.getHeight();
        _barWidth = hpBarRaw.getWidth();
        _barValueHeight = _barHeight - 10;
        _barValueWidth = _barWidth - 40;

        BufferedImage[] hpBarHudRaw = new BufferedImage[]{hpBarRaw, hpBarRaw};
        _hpBar = new TLabel("hp", HP_BAR_X, HP_BAR_Y,
                _barWidth, _barHeight);
        _hpBar.setExternalUI(hpBarHudRaw, false);

        /*Bar value*/
        BufferedImage hpBarValue = drawBarValue(
                _barValueWidth, _barValueHeight,
                _hpBarValue, healthColor, healthBgColor);
        BufferedImage[] hpBarValueRaw = new BufferedImage[]{hpBarValue, hpBarValue};

        _hpBarState = new TLabel("hp_value", HP_BAR_X + 20, HP_BAR_Y,
                hpBarRaw.getWidth() - 10, hpBarRaw.getHeight() - 10);
        _hpBarState.setExternalUI(hpBarValueRaw, false);

        this.add(_hpBarState);
        this.add(_hpBar);

        /*Mana-points bar initialization*/
        BufferedImage mpBarRaw = _loader.getStoredImage(MP_BAR_PATH);
        BufferedImage[] mpBarHudRaw = new BufferedImage[]{mpBarRaw, mpBarRaw};
        _mpBar = new TLabel("hp", MP_BAR_X, MP_BAR_Y,
                mpBarRaw.getWidth(), mpBarRaw.getHeight());
        _mpBar.setExternalUI(mpBarHudRaw, false);

        /*Bar value*/
        BufferedImage mpBarValue = drawBarValue(
                hpBarRaw.getWidth() - 40, hpBarRaw.getHeight() - 10,
                100, manaColor, manaBgColor);
        BufferedImage[] mpBarValueRaw = new BufferedImage[]{mpBarValue, mpBarValue};

        _mpBarState = new TLabel("mp_value", MP_BAR_X + 20, MP_BAR_Y,
                mpBarRaw.getWidth() - 10, mpBarRaw.getHeight() - 10);
        _mpBarState.setExternalUI(mpBarValueRaw, false);

        this.add(_mpBarState);
        this.add(_mpBar);

        /*Animated health-point dragon initialization*/
        _hpDragon = new TAnimatedComponent(HP_BAR_X, HP_BAR_Y - 68,
                _loader.getStoredImages(HP_DRAGON_PATH), 3);
        this.add(_hpDragon);

        /*Animated mana-point dragon initialization*/
        _mpDragon = new TAnimatedComponent(MP_BAR_X, MP_BAR_Y - 68,
                _loader.getStoredImages(MP_DRAGON_PATH), 3);
        this.add(_mpDragon);
    }

    private BufferedImage drawBarValue(int width, int height, int value, Color primary, Color secondary) {
        BufferedImage barValue = ImageUtil.createImage(width, height);
        Graphics2D canvas = barValue.createGraphics();

        int offset = (int) (height * ((double) (100 - value) / 100));

        canvas.setColor(secondary);
        canvas.fillRect(0, 0, width, offset);

        canvas.setColor(primary);
        canvas.fillRect(0, offset, width, height - offset);

        return barValue;
    }

    private void initWandBar() {
        BufferedImage wandbarPix = _loader.getStoredImage(WAND_BAR_PATH);
        _wandbarPix = new TLabel("", 110, 670, wandbarPix.getWidth(), wandbarPix.getHeight());
        _wandbarPix.setExternalUI(new BufferedImage[]{wandbarPix, wandbarPix}, false);
        this.add(_wandbarPix);

        _spells = new TImageButton[5];
        
        BufferedImage[] spells = {
            _loader.getStoredImage(DIFFINDO_PATH),
            _loader.getStoredImage(PROTEGO_PATH),
            _loader.getStoredImage(EXPILLIARMUS_PATH),
            _loader.getStoredImage(FERULA_PATH),
            _loader.getStoredImage(APPARTION_PATH)
        };

        String[] spellNames = {
            "Diffindo",
            "Protego",
            "Expelliarmus",
            "Ferula",
            "Apparition"
        };

        int startButtonX = 458;
        int buttonOffset = 82;
        for (int i = 0; i < spells.length; ++i) {
            _spells[i] = new TImageButton(
                    spellNames[i], spells[i],
                    startButtonX + buttonOffset*i, 675, spells[i].getWidth(), spells[i].getHeight()){
                        @Override
                        public void doAction(){
                            _modelListener.OnBarButtonEvent(this);
                        }
                    };
            this.add(_spells[i]);
        }

    }

    public void setSelectorState(boolean value) {
        for (TSelectorButton[] buttonRow : _selector) {
            for (TSelectorButton button : buttonRow) {
                button.setEnabled(value);
            }
        }
        
        inSelectorMode = value;
        
        for (int i = 0; i < _spells.length -1; ++i){
            _spells[i].setEnabled(!value);
        }
    }
    
    public boolean getSelectorState(){
        return inSelectorMode;
    }
//        

    private final static int TOP_X = 5;
    private final static int TOP_Y = 5;
    private final static int BUTTON_WIDTH = 150;
    private final static int BUTTON_HEIGHT = 30;
    private final static int HP_BAR_X = 15;
    private final static int HP_BAR_Y = 240;
    private final static int MP_BAR_X = 919;
    private final static int MP_BAR_Y = 240;

    private final static String HP_BAR_PATH = "pix/hud/hpbar.png";
    private final static String MP_BAR_PATH = "pix/hud/mpbar.png";
    private final static String HP_DRAGON_PATH = "anim/hp_dragon";
    private final static String MP_DRAGON_PATH = "anim/mp_dragon";
    private final static String WAND_BAR_PATH = "pix/hud/wandbar";

    private final static String DIFFINDO_PATH = "pix/hud/diffindo";
    private final static String PROTEGO_PATH = "pix/hud/protego";
    private final static String EXPILLIARMUS_PATH = "pix/hud/expilliarmus";
    private final static String FERULA_PATH = "pix/hud/ferula";
    private final static String APPARTION_PATH = "pix/hud/apparition";

    @Override
    public String UIName() {
        return "MazeUI";
    }
}
