/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package view.gui;

import com.golden.gamedev.gui.TButton;

/**
 *
 * @author Alexander Lyashenko
 */
public class TSelectorButton extends TButton{

    public TSelectorButton(String string, int x, int y, int w, int h) {
        super(string, x, y, w, h);
    }
    
    public String UIName() {
        return "Selector button";
    }
}
