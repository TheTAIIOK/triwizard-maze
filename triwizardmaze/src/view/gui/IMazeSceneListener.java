/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.gui;

import maze.Maze.EndReason;

/**
 *
 * @author still
 */
public interface IMazeSceneListener {
    public void gameEnded(EndReason reason);
}
