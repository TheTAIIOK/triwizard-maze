/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.toolkit.FrameWork;
import com.golden.gamedev.gui.toolkit.TContainer;
import com.golden.gamedev.object.AnimatedSprite;
import com.golden.gamedev.object.background.ImageBackground;
import java.awt.Graphics2D;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import maze.Maze;
import triwizardmaze.themegtge.TriwizardmazeTheme;
import view.gui.GDialogScreen;
import view.gui.GEndGameScreen;
import view.gui.GHelpScreen;
import view.gui.GMainScreen;
import view.gui.IBasicSceneListener;
import view.gui.TRoleButton;
import view.gui.TRoleButton.ButtonRole;

/**
 *
 * @author godric
 */
public class MainMenu extends GameObject{

    private ImageBackground _imgbg;

    private FrameWork _frame;     // main frame

    private AnimatedSprite _flame;

    private HashMap<GuiState, TContainer> _uiStates;

    private boolean _isCorrupt;
    
    private boolean _isMissing;
    
    public MainMenu(GameEngine ge, boolean isCorruptedFound) {
        super(ge);
        _uiStates = new HashMap<GuiState, TContainer>();
        _isCorrupt = isCorruptedFound;
    }

    @Override
    public void initResources() {
        _imgbg = new ImageBackground(bsLoader.getStoredImage("pix/background/menu.png"));

        _flame = new AnimatedSprite(bsLoader.getStoredImages("anim/flame_"), 196, 246);
        _flame.getAnimationTimer().setDelay(10);
        _flame.setAnimationFrame(0, 31);
        _flame.setAnimate(true);
        _flame.setLoopAnim(true);

        _frame = new FrameWork(bsInput, getWidth(), getHeight());
        _frame.installTheme(new TriwizardmazeTheme());

        _uiStates.put(GuiState.MENU, new GMainScreen(new IBasicSceneListener() {
            @Override
            public void OnButtonEvent(TButton initiator){
                try {
                    OnButtonClicked(initiator);
                } catch (IOException ex){
                    parent.nextGameID = 2;
                    finish();
                } 
            }
        }));
        
         _uiStates.put(GuiState.DIALOG, new GDialogScreen(new IBasicSceneListener() {
            @Override
            public void OnButtonEvent(TButton initiator) {
                OnDialogButtonClicked((TRoleButton)initiator);
            }
        }));
         
         _uiStates.values().forEach((ui) -> {
            ui.setEnabled(false);
        });
         
         _uiStates.put(GuiState.HELP, new GHelpScreen(new IBasicSceneListener() {
            @Override
            public void OnButtonEvent(TButton initiator) {
                switchGui(GuiState.MENU);
            }
        }));
         
         _uiStates.values().forEach((ui) -> {
            ui.setEnabled(false);
        });
         
        if (_isCorrupt){
            ((GDialogScreen)_uiStates.get(GuiState.DIALOG))
                            .showDialog("Load", "Save is corrupted. \nStart new game?", "Yes", "No");
            switchGui(GuiState.DIALOG);
        } else {
            switchGui(GuiState.MENU);
        }
            
    }

    @Override
    public void update(long l) {
        _frame.update();
        _flame.update(l);
    }

    @Override
    public void render(Graphics2D gd) {
        this._imgbg.render(gd);
        _frame.render(gd);
        _flame.render(gd);

    }

    private void OnButtonClicked(TButton initiator) throws IOException{
        String text = initiator.getText();

        switch (text) {
            case "New Game":
                //Файл существует
                ObjectInputStream oi = new ObjectInputStream(new FileInputStream("gamesave.cer"));
                
                ((GDialogScreen)_uiStates.get(GuiState.DIALOG))
                            .showDialog("New Game", "Save found. Continue game?", "Yes", "No");
                    switchGui(GuiState.DIALOG);
                break;
            case "Load":
                parent.nextGameID = 10;
                 finish();
                break;
            case "Profile":
                parent.nextGameID = 9;
                finish();
                break;
            case "Help":
                switchGui(GuiState.HELP);
                break;
            case "Exit to menu":
                parent.nextGameID = -1;
                finish();
                break;

            default:;
        }
    }
    
    private void OnDialogButtonClicked(TRoleButton button) {
        
        switch (button.getMentorName()) {
            case "New Game":
                if (button.getRole().equals(ButtonRole.ACCEPT)){
                    parent.nextGameID = 10;
                } else {
                    parent.nextGameID = 2;
                }
                finish();
                break;
                
            case "Load":
                if (button.getRole().equals(ButtonRole.ACCEPT)){
                    parent.nextGameID = 2;
                    finish();
                } else {
                    switchGui(GuiState.MENU);
                }
                break;
                        
            case "Exit to menu":
                if (button.getRole().equals(ButtonRole.ACCEPT)){
                parent.nextGameID = -1;
                finish();
                }
                break;

            default:;
        }
    }
    private void switchGui(GuiState guiState) {
        _frame.getContentPane().setEnabled(false);
        _frame.setContentPane(_uiStates.get(guiState));
        _frame.getContentPane().setEnabled(true);
    }

    private static enum GuiState {
        MENU,
        DIALOG,
        HELP
    }
}
