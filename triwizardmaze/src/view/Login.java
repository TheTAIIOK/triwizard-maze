/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import game.Triwizardmaze;
import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.gui.TLabel;
import com.golden.gamedev.gui.TTextField;
import com.golden.gamedev.gui.toolkit.FrameWork;
import com.golden.gamedev.object.background.ImageBackground;
import game.PlayerStatistic;
import java.awt.Graphics2D;
import sqlrequest.ServerConnector;

/**
 *
 * @author godric
 */
public class Login extends GameObject{

    private ImageBackground _imgbg;
    
    private FrameWork 	_frame;     // main frame
    private TLabel _lglabel;
    private TTextField _login;      // login text
    
    public Login(GameEngine ge) {
        super(ge);     
    }
    
    @Override
    public void initResources() {
        _imgbg = new ImageBackground(bsLoader.getStoredImage("pix/background/menu.png"));
        
        _frame = new FrameWork(bsInput, getWidth(), getHeight());
        
        // Login label.
        _lglabel = new TLabel("login", 380, 180, 40, 20);
        _frame.add(_lglabel);
        
        // Login text field.
        _login = new TTextField("Enter your name and press Enter", 620, 380, 300, 30) {
            private boolean first_key_press = true;
            @Override
            public void doAction() {
                String playerNickname = this.getText();
                
                if (playerNickname.isEmpty()) {
                    this.setText("Empty name is not allowed! Try again");
                    this.first_key_press = true;
                    return;
                }
                if (ServerConnector.canLogin(playerNickname) != 0) {
                    //TODO: Check if new player or not. Load cloud save
                    PlayerStatistic plst = ServerConnector.getMyStatistic(playerNickname);
                    if (plst != null) {
                        Triwizardmaze.setPlayerInstance(plst);
                        //Switch to main menu.
                        parent.nextGameID = 1;
                        finish();
                    } else {
                        this.setText("please check your connection and try again!");
                        this.first_key_press = true;
                    }
                } else {
                    // Login fail.
                    this.setText("please check your connection and try again!");
                    this.first_key_press = true;
                }
            }
            
            @Override
            protected void processKeyPressed() {
                if (first_key_press) {
                    first_key_press = false;
                    this.setText("");
                }
                super.processKeyPressed();
            }
        };
        _login.setToolTipText("please enter your login");
        _login.setEditable(true);
        _frame.add(_login);
        
    }

    @Override
    public void update(long l) {
        _frame.update();
        _login.requestFocus();
    }

    @Override
    public void render(Graphics2D gd) {
        this._imgbg.render(gd);
       _frame.render(gd);
    }
    
}
