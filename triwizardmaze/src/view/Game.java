/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import game.GameSetting;
import maze.Wall;
import maze.Wizard;
import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.toolkit.FrameWork;
import com.golden.gamedev.gui.toolkit.TContainer;
import com.golden.gamedev.object.background.ImageBackground;
import com.golden.gamedev.util.ImageUtil;
import controllers.CastingPlayerAI;
import controllers.MovingAIWizardController;
import controllers.GobletController;
import controllers.MovingAI;
import controllers.MovingAIMonsterController;
import controllers.MovingPlayerController;
import game.Triwizardmaze;
import maze.Goblet;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import magic.Perricullum;
import magic.Protego;
import maze.AbstrObject;
import maze.Character;
import maze.Enemy;
import maze.FakeGoblet;
import maze.InvincibleMonster;
import maze.Maze;
import maze.SimpleMonster;
import navigation.CellPosition;
import navigation.Direction;
import navigation.MiddlePosition;
import sqlrequest.ServerConnector;
import triwizardmaze.themegtge.TriwizardmazeTheme;
import view.gui.GDialogScreen;
import view.gui.GEndGameScreen;
import view.gui.GGameScreen;
import view.gui.GHelpScreen;
import view.gui.GPauseScreen;
import view.gui.IMazeSceneListener;
import view.gui.TRoleButton;
import view.gui.TRoleButton.ButtonRole;
import view.gui.TImageButton;
import view.gui.IBasicSceneListener;
import view.gui.IMazeViewListener;

/**
 *
 * @author godric
 */
public class Game extends GameObject {

    private FrameWork _frame;

    private Maze _maze;

    private boolean isPause;
    
    private boolean _teleport;

    private HashMap<GuiState, TContainer> _uiStates;

    public Game(GameEngine ge) throws IOException, ClassNotFoundException{
        super(ge);

        calculateConstantsAndInits();
        CellPosition.setHorizontalRange(1, GameSetting.WIDTH);
        CellPosition.setVerticalRange(1, GameSetting.HEIGHT);
        
        _maze = new Maze(this, new IMazeSceneListener() {
            @Override
            public void gameEnded(Maze.EndReason reason) {
                OnGameEnded(reason);
            }
        });

        isPause = false;
        _teleport = false;
        _maze.setPause(isPause);
    }

    public Game(GameEngine ge, GameSetting.Heroes hero) {
        super(ge);

        calculateConstantsAndInits();
        CellPosition.setHorizontalRange(1, GameSetting.WIDTH);
        CellPosition.setVerticalRange(1, GameSetting.HEIGHT);

        _maze = new Maze(hero, new IMazeSceneListener() {
            @Override
            public void gameEnded(Maze.EndReason reason) {
                OnGameEnded(reason);
            }
        });
        isPause = false;
        _maze.setPause(isPause);
    }

    public void calculateConstantsAndInits() {
        GameSetting.CHAR_WIDTH = (GameSetting.MAZE_WIDTH - GameSetting.WALL_THIN * (GameSetting.WIDTH + 1)) / (GameSetting.WIDTH);
        GameSetting.CHAR_HEIGHT = (GameSetting.MAZE_HEIGHT - GameSetting.WALL_THIN * (GameSetting.HEIGHT + 1)) / (GameSetting.HEIGHT);
        GameSetting.MAZE_WIDTH = GameSetting.WALL_THIN + (GameSetting.WALL_THIN + GameSetting.CHAR_WIDTH) * GameSetting.WIDTH;
        GameSetting.MAZE_HEIGHT = GameSetting.WALL_THIN + (GameSetting.WALL_THIN + GameSetting.CHAR_HEIGHT) * GameSetting.HEIGHT;
        GameSetting.MAZE_X_LEFT = (getWidth() - GameSetting.MAZE_WIDTH) / 2;
        GameSetting._imgbg = new ImageBackground(bsLoader.getStoredImage("pix/background/maze.png"));
    }

    // ------- init -------
    @Override
    public void initResources() {
        setupUi();
        setupWizards();
        setupTraps();
        setupGoblets();
        setupFakeGoblets();
        setupEnemies();
        _maze.setupCollisions();
    }

    private void setupTraps() {
        // Set images for each hero.
        List<AbstrObject> tmp = _maze.getWorld().objects(maze.Trap.class);
        BufferedImage bi = ImageUtil.resize(bsLoader.getStoredImage("pix/trap.png"),
                GameSetting.CHAR_WIDTH, GameSetting.CHAR_HEIGHT);
        tmp.stream().forEach((trap) -> {
            trap.setImage(bi);
        });
    }

    private void setupGoblets() {

        // --- goblet
        List<AbstrObject> tmp = _maze.getWorld().objects(maze.Goblet.class);
        BufferedImage bi = ImageUtil.resize(bsLoader.getStoredImage("pix/cup.png"),
                GameSetting.CHAR_WIDTH, GameSetting.CHAR_HEIGHT);
        tmp.stream().forEach((cap) -> {
            cap.setImage(bi);

            _maze.controllers.add(new GobletController(this, cap));
        });
    }

    private void setupFakeGoblets() {

        // --- goblet
        List<AbstrObject> tmp = _maze.getWorld().objects(maze.FakeGoblet.class);
        BufferedImage bi = ImageUtil.resize(bsLoader.getStoredImage("pix/cup.png"),
                GameSetting.CHAR_WIDTH, GameSetting.CHAR_HEIGHT);
        tmp.stream().forEach((cap) -> {
            cap.setImage(bi);

            _maze.controllers.add(new GobletController(this, cap));
        });
    }

    private void setupWizards() {
        // Set images for each hero.
        List<AbstrObject> tmp = _maze.getWorld().objects(Wizard.class);
        CellPosition p;
        Wizard wizard;
        BufferedImage bi = null;
        BufferedImage[] bis = null;
        for (AbstrObject hr : tmp) {
            wizard = (Wizard) hr;
            p = wizard.position();
            // ----- set image
            switch (wizard.getName()) {
                case HARRY:
                    //bi = bsLoader.getStoredImage("pix/player/harry.png");
                    bis = bsLoader.getStoredImages("pix/player/potter");
                    break;
                case VICTOR:
                    //bi = bsLoader.getStoredImage("pix/player/victor.png");
                    bis = bsLoader.getStoredImages("pix/player/krum");
                    break;
                case FLEUR:
                   // bi = bsLoader.getStoredImage("pix/player/fleur.png");
                    bis = bsLoader.getStoredImages("pix/player/delacour");
                    break;
                case CEDRIC:
                   // bi = bsLoader.getStoredImage("pix/player/cedric.png");
                    bis = bsLoader.getStoredImages("pix/player/diggory");
                    break;
            }
//            wizard.setImage(ImageUtil.resize(bi,
//                    GameSetting.CHAR_WIDTH - 1, GameSetting.CHAR_HEIGHT - 1));
            wizard.setDirectionImages(bis);
            Point cp = Game.CellToPoint(p);
            wizard.setLocation(cp.getX(), cp.getY());

            // ---- set controller
            if (_maze._hero == wizard) {
                _maze.controllers.add(new MovingPlayerController(this, wizard));
                _maze.controllers.add(new CastingPlayerAI(this, wizard));
            } else {
                _maze.controllers.add(new MovingAIWizardController(this, wizard));
            }
        }
    }
    
    
    private void setupEnemies() {
        List<AbstrObject> tmp = _maze.getWorld().objects(maze.InvincibleMonster.class);
        BufferedImage bi1 = ImageUtil.resize(bsLoader.getStoredImage("pix/enemy1.png"),
                    GameSetting.CHAR_WIDTH - 1, GameSetting.CHAR_HEIGHT - 1);
        tmp.stream().forEach((monster) -> {
            
                monster.setImage(bi1);
            _maze.controllers.add(new  MovingAIMonsterController(this, (Enemy) monster));
        });
        
        List<AbstrObject> stmp = _maze.getWorld().objects(maze.SimpleMonster.class);
        BufferedImage bi2 = ImageUtil.resize(bsLoader.getStoredImage("pix/enemy2.png"),
                    GameSetting.CHAR_WIDTH - 1, GameSetting.CHAR_HEIGHT - 1);
        
        stmp.stream().forEach((monster) -> {
            
                monster.setImage(bi2);
            _maze.controllers.add(new  MovingAIMonsterController(this, (Enemy) monster));
        });
    }

    private void setupUi() {
        _uiStates = new HashMap(3);
        _frame = new FrameWork(bsInput, getWidth(), getHeight());
        _frame.installTheme(new TriwizardmazeTheme());

        _uiStates.put(GuiState.GAME, new GGameScreen(
                new IMazeViewListener() {
            @Override
            public void OnButtonEvent(TButton intiator) {
                if (intiator.getText().equals("Pause")) {
                    switchGui(GuiState.PAUSE);
                    isPause = true;
                    _maze.setPause(isPause);
                    stopTimers();
                }
            }

            @Override
            public void OnBarButtonEvent(TImageButton button) {
                System.out.printf("Mammamia, called " + button.getText() + "\n");
                switch(button.getText()){
                    case "Diffindo":
                        _maze.getHero().makeSpell(magic.Diffindo.class);
                        break;
                    case "Ferula":
                        _maze.getHero().makeSpell(magic.Ferula.class);
                        break;
                    case "Protego":
                        _maze.getHero().makeSpell(magic.Protego.class);
                        break;
                    case "Expelliarmus":
                        _maze.getHero().makeSpell(magic.Expelliarmus.class);
                        break;
                    case "Apparition":
                       if (((GGameScreen)_frame.getContentPane()).getSelectorState()){
                           _teleport = false;
                            ((GGameScreen)_frame.getContentPane()).setSelectorState(false);
                       }
                        else{
                           _teleport = true;
                            ((GGameScreen)_frame.getContentPane()).setSelectorState(true);
                       }
                        break;
                    default: break;
                }
            }

            @Override
            public void selectorClicked(int x, int y) {
                //Here we need to cast Appartion spell
                _maze.getHero().teleport(x, y);
                _teleport = false;
                System.out.print(x + " " + y + "\n");
            }
            
        }, _maze.getHero(), this.bsLoader));

        _uiStates.put(GuiState.PAUSE, new GPauseScreen(
                new IBasicSceneListener() {
            @Override
            public void OnButtonEvent(TButton button) {
                OnButtonClicked(button.getText());
            }
        }));

        _uiStates.put(GuiState.DIALOG, new GDialogScreen(
                new IBasicSceneListener() {
            @Override
            public void OnButtonEvent(TButton button) {
                OnDialogButtonClicked((TRoleButton) button);
            }
        }));

        _uiStates.put(GuiState.END, new GEndGameScreen(
                new IBasicSceneListener() {
            @Override
            public void OnButtonEvent(TButton button) {
                OnButtonClicked(button.getText());
            }
        }, this.bsLoader));

         _uiStates.put(GuiState.HELP, new GHelpScreen(new IBasicSceneListener() {
            @Override
            public void OnButtonEvent(TButton initiator) {
                switchGui(GuiState.PAUSE);
            }
        }));
         
        _uiStates.values().forEach((ui) -> {
            ui.setEnabled(false);
        });

        _uiStates.get(GuiState.GAME).setEnabled(true);
        _frame.setContentPane(_uiStates.get(GuiState.GAME));
    }

    @Override
    public void update(long l) {
        _frame.update();

        
        if(this.keyPressed(java.awt.event.KeyEvent.VK_ESCAPE) && !isPause){
            switchGui(GuiState.PAUSE);
            isPause = true;
            _maze.setPause(isPause);
            stopTimers();
        } else if(this.keyPressed(java.awt.event.KeyEvent.VK_ESCAPE) && isPause){
            switchGui(GuiState.GAME);
            isPause = false;
            _maze.setPause(isPause);
            startTimers();
        }
        
        
        if (!isPause) {
            _maze.update(l);

        }
    }

    // ------- render -------
    @Override
    public void render(Graphics2D gd) {

        if (!isPause) {
            _maze.getWorld().render(gd);
            renderWorld(gd);
            _maze.getWorld().getGroup("Magics").render(gd);
        } else {
            GameSetting._imgbg.render(gd);
        }

        _frame.render(gd);
    }

    private void renderWorld(Graphics2D gd) {
        gd.setColor(Color.getHSBColor(30, 100, 59));
        
        renderWalls(gd);
        renderImgObjects(gd, Wizard.class);
        renderImgObjects(gd, Goblet.class);
        renderImgObjects(gd, FakeGoblet.class);
        renderImgObjects(gd, maze.Trap.class);
        renderImgObjects(gd, maze.SimpleMonster.class);
        renderImgObjects(gd, maze.InvincibleMonster.class);
    }

    private void renderWalls(Graphics2D gd) {
        gd.setColor(new Color(46,70,59));
        //------------------------------------------------------
        _maze.getWalls().stream().forEach((wall) -> {
            drawWall(gd, wall);
        });
        // draw top
        gd.fillRect(GameSetting.MAZE_X_LEFT, GameSetting.MAZE_Y_TOP-4, GameSetting.MAZE_WIDTH, GameSetting.WALL_THIN);
//         draw right
        gd.fillRect(GameSetting.MAZE_X_LEFT + GameSetting.MAZE_WIDTH - GameSetting.WALL_THIN,
                GameSetting.MAZE_Y_TOP, GameSetting.WALL_THIN, GameSetting.MAZE_HEIGHT);
//         draw bott
        gd.fillRect(GameSetting.MAZE_X_LEFT, GameSetting.MAZE_Y_TOP+ 4 + GameSetting.MAZE_HEIGHT - GameSetting.WALL_THIN,
                GameSetting.MAZE_WIDTH, GameSetting.WALL_THIN);
//         draw left
        gd.fillRect(GameSetting.MAZE_X_LEFT, GameSetting.MAZE_Y_TOP, GameSetting.WALL_THIN, GameSetting.MAZE_HEIGHT);

    }

    private void drawWall(Graphics2D gd, Wall wall) {
        MiddlePosition pos = wall.position();
        int lx = GameSetting.CHAR_WIDTH + GameSetting.WALL_THIN, ly = GameSetting.CHAR_HEIGHT + GameSetting.WALL_THIN;
        Point p = MiddleToPoint(wall.position());
        int x = (int) p.getX() - GameSetting.WALL_THIN, y = (int) p.getY() - GameSetting.WALL_THIN;
        wall.setLocation(x, y);
        
        if (pos.direction().equals(Direction.west())){
            wall.setImage(bsLoader.getStoredImage("pix/wall_west"));
        } else if (pos.direction().equals(Direction.north())) {
            wall.setImage(bsLoader.getStoredImage("pix/wall_north"));
        } else if (pos.direction().equals(Direction.east())) {
            wall.setImage(bsLoader.getStoredImage("pix/wall_east"));
        } else {
            wall.setImage(bsLoader.getStoredImage("pix/wall_south"));
        }
        
        wall.render(gd);
    }

    private void renderImgObjects(Graphics2D gd, Class objClass) {
        List<AbstrObject> wizards = _maze.getWorld().objects(objClass);
        wizards.stream().forEach((w) -> {
            w.render(gd);
        });
    }

    public static Point CellToPoint(CellPosition cell) {
        int x = GameSetting.MAZE_X_LEFT + GameSetting.WALL_THIN
                + (cell.column() - 1) * (GameSetting.CHAR_WIDTH + GameSetting.WALL_THIN);
        int y = GameSetting.MAZE_Y_TOP + GameSetting.WALL_THIN
                + (cell.row() - 1) * (GameSetting.CHAR_HEIGHT + GameSetting.WALL_THIN);
        return new Point(
                GameSetting.MAZE_X_LEFT + GameSetting.WALL_THIN
                + (cell.column() - 1) * (GameSetting.CHAR_WIDTH + GameSetting.WALL_THIN),
                GameSetting.MAZE_Y_TOP + GameSetting.WALL_THIN
                + (cell.row() - 1) * (GameSetting.CHAR_HEIGHT + GameSetting.WALL_THIN));
    }

    public static Point MiddleToPoint(MiddlePosition mid) {
        return CellToPoint(mid.cellPosition());
    }

    public Maze getMaze() {
        return _maze;
    }

    /**
     * Handler for UI events
     *
     * @param buttonName button-initiator of event
     * @param buttonArgument argument, send by initiator (needs for few call)
     */
    private void OnButtonClicked(String buttonName) {
        switch (buttonName) {
            case "Load":
                switchGui(GuiState.DIALOG);
                ((GDialogScreen) _uiStates.get(GuiState.DIALOG)).showDialog("Load", "Are you sure to load?", "Yes", "No");
                break;

            case "Back":
                switchGui(GuiState.GAME);
                isPause = false;
                startTimers();
                _maze.setPause(isPause);
                
                break;

            case "Save":
                switchGui(GuiState.DIALOG);
                ((GDialogScreen) _uiStates.get(GuiState.DIALOG)).showDialog("Save", "Are you sure to save?", "Yes", "No");
                break;

            case "Help":
                /*Load help screen - NOT IMPLEMENTED*/
                switchGui(GuiState.HELP);
                break;

            case "Give up":
                switchGui(GuiState.DIALOG);
                ((GDialogScreen) _uiStates.get(GuiState.DIALOG)).showDialog("Give up", "Are you sure to give up?", "Yes", "No");
                break;

            case "Back to menu":
                switchGui(GuiState.DIALOG);
                ((GDialogScreen) _uiStates.get(GuiState.DIALOG)).showDialog("Back to menu", "Are you sure to exit?", "Yes", "No");
                break;

            case "Exit to menu":
                switchGui(GuiState.DIALOG);
                ((GDialogScreen) _uiStates.get(GuiState.DIALOG)).showDialog("Exit to menu", "Are you sure to exit?", "Yes", "No");
                break;

            default:;
        }
    }

    private void OnDialogButtonClicked(TRoleButton button) {
        switch (button.getMentorName()) {
//            case "Load":
//                if (button.getRole().equals(ButtonRole.ACCEPT)) {
//                    boolean IsLoadSuccesful = _maze.loadGame(); // Load call
//
//                    if (!IsLoadSuccesful) {
//                        switchGui(GuiState.PAUSE);
//                        ((GPauseScreen) _frame.getContentPane()).showInfoMessage("Can't load save");
//                    } else {
//                        switchGui(GuiState.GAME);
//                        isPause = false;
//                        _maze.setPause(isPause);
//                    }
//                } else {
//                    switchGui(GuiState.PAUSE);
//                }
//                break;

            case "Save":
                if (button.getRole().equals(ButtonRole.ACCEPT)) {
                    boolean IsSaveSuccessful = _maze.saveGame(); // Save call
                    boolean IsProblemInConnection = false; //This flag need to be set if there is connection problem

                    String message = IsSaveSuccessful
                            ? "Save successfull"
                            : (IsProblemInConnection
                                    ? "Can't save game, check connection to the Web"
                                    : "Can't save game, some problems occured");

                    switchGui(GuiState.PAUSE);
                    ((GPauseScreen) _frame.getContentPane()).showInfoMessage(message);

                } else {
                    switchGui(GuiState.PAUSE);
                }
                break;

            case "Give up":
                if (button.getRole().equals(ButtonRole.ACCEPT)) {
                    switchGui(GuiState.GAME);
                    isPause = false;
                    _maze.setPause(isPause);
                    _maze._hero.makeSpell(Perricullum.class);
                    
                } else {
                    switchGui(GuiState.PAUSE);
                }
                break;

            case "Exit to menu":
                if (button.getRole().equals(ButtonRole.ACCEPT)) {
                    parent.nextGameID = 1;
                    finish();
                } else {
                    switchGui(GuiState.PAUSE);
                }
                break;

            case "Back to menu":
                if (button.getRole().equals(ButtonRole.ACCEPT)) {
                    parent.nextGameID = 1;
                    finish();
                } else {
                    switchGui(GuiState.END);
                }
                break;
            default:;
        }
    }

    private void switchGui(GuiState guiState) {
        _frame.getContentPane().setEnabled(false);
        _frame.setContentPane(_uiStates.get(guiState));
        _frame.getContentPane().setEnabled(true);
    }

    private void OnGameEnded(Maze.EndReason reason) {
        ServerConnector.updatePlayer(Triwizardmaze.getPlayerInstance());
        switchGui(GuiState.END);
        ((GEndGameScreen) _uiStates.get(GuiState.END)).setup(reason);
    }

    private static enum GuiState {
        GAME,
        PAUSE,
        DIALOG,
        END, 
        HELP
    }
    
    private void stopTimers(){
        List<AbstrObject> wizards = _maze.getWorld().objects(Wizard.class);
        if(!wizards.isEmpty()){
            wizards.stream().forEach((w) -> {
                ((Wizard)w).pause();
            });
        }
        
        List<AbstrObject> protegos = _maze.getWorld().objects(Protego.class);
        if(!protegos.isEmpty()){
            protegos.stream().forEach((p) -> {
                ((Protego)p).pause();
            });
        }
        
    }
    
    private void startTimers(){
        
        List<AbstrObject> wizards = _maze.getWorld().objects(Wizard.class);
        if(!wizards.isEmpty()){
            wizards.stream().forEach((w) -> {
                ((Wizard)w).resume();
            });
        }
        
        
        List<AbstrObject> protegos = _maze.getWorld().objects(Protego.class);
        if(!protegos.isEmpty()){
            protegos.stream().forEach((p) -> {
                ((Protego)p).resume();
            });
        }
        
    }
    
    public FrameWork getFrame(){
        return _frame;
    }
    
    public boolean getTeleport(){
        return _teleport;
    }
    
    public void setTeleport(boolean t){
        _teleport=t;
    }
}
