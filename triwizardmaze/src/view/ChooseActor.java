/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.TLabel;
import com.golden.gamedev.gui.toolkit.FrameWork;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.background.ImageBackground;
import java.awt.Graphics2D;
import triwizardmaze.themegtge.TriwizardmazeTheme;
import view.gui.TMultilineButton;

/**
 *
 * @author godric
 */
public class ChooseActor extends GameObject {

    private final static int BUTTON_X = 30;
    private final static int BUTTON_Y = 200;
    private final static int BUTTON_WIDTH = 250;
    private final static int BUTTON_HEIGHT = 100;
    private final static int BUTTON_SPACE = BUTTON_HEIGHT + 30;
    
    
    private ImageBackground _imgbg;
    
    private FrameWork 	_frame;     // main frame

    private TButton _btHarry;
    private TButton _btCedric;
    private TButton _btFleur;
    private TButton _btVictor;
    
    private Sprite _sHarry;
    private Sprite _sCedric;
    private Sprite _sFleur;
    private Sprite _sVictor;
    
    public ChooseActor(GameEngine ge) {
        super(ge);
    }

    @Override
    public void initResources() {
        TLabel img;
        _imgbg = new ImageBackground(bsLoader.getStoredImage("pix/background/choose.png"));
        
        _frame = new FrameWork(bsInput, getWidth(), getHeight());
        _frame.installTheme(new TriwizardmazeTheme());
        
        // add sprites
        _sHarry = new Sprite(bsLoader.getStoredImage("pix/player/harry-full.png"));
        _sHarry.setX(150);
        _sHarry.setY(350);
        
        
        _sVictor = new Sprite(bsLoader.getStoredImage("pix/player/victor-full.png"));
        _sVictor.setX(350);
        _sVictor.setY(350);
        
        _sFleur = new Sprite(bsLoader.getStoredImage("pix/player/fleur-full.png"));
        _sFleur.setX(550);
        _sFleur.setY(350);
        
        _sCedric = new Sprite(bsLoader.getStoredImage("pix/player/cedric-full.png"));
        _sCedric.setX(750);
        _sCedric.setY(350);

        // add actors
        _btHarry = addActor(95, "Harry\nPotter", 3);
        _btVictor = addActor(285, "Victor\nKrum", 4);
        _btFleur = addActor(490, "Fleur\nDelacour", 5);
        _btCedric = addActor(690, "Cedric\nDiggory", 6);
        
        // back buttons
        _frame.add(new TButton("Back", 
                getWidth()-BUTTON_WIDTH -5 , getHeight()-BUTTON_HEIGHT-10, 
                BUTTON_WIDTH, BUTTON_HEIGHT-10) {
            @Override
            public void doAction() {
                parent.nextGameID = 1;
                finish();
            }
        });
        
        //add status
        
    }

    @Override
    public void update(long l) {
        _frame.update();
    }

    @Override
    public void render(Graphics2D gd) {
        this._imgbg.render(gd);
        _sHarry.render(gd);
        _sCedric.render(gd);
        _sFleur.render(gd);
        _sVictor.render(gd);
        _frame.render(gd);
    }
    
    private TButton addActor(int x, String name, int nextGameId) {
        TButton bt = new TMultilineButton(name, x, BUTTON_Y, BUTTON_WIDTH, BUTTON_HEIGHT){
            @Override
            public void doAction() {
                parent.nextGameID = nextGameId;
                finish();
            }
        };
        
        _frame.add(bt);
        return bt;
    }
}
