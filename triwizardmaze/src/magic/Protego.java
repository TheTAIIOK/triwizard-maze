/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magic;

import maze.Wizard;
import com.golden.gamedev.object.Timer;
import maze.World;
import controllers.Stoppable;

/**
 *
 * @author godric
 */
public final class Protego extends DefendingMagic implements Stoppable{
    
    private static String IMG_PATH = "resources/pix/magic/protego.png";
    private Timer timer;

    public Protego(World world, Wizard mk) {
        super(world, mk, 20);
        _paused = false;
        setImage(IMG_PATH);
        timer = new Timer(2000);
        this._cost = 20;
    }

    @Override
    public void update(long l) {
        if(!_paused){
            super.update(l);
            this.setPosition(this._maker.position());
            if (timer.action(l)) {
                this._maker.setProtection(0);
                this._world.removeObject(this);
            }
        }
    }

     @Override
    public boolean castASpell() {
        if(super.isCapable()){
            
            this.setPosition(this._maker.position());
            this._maker.setProtection(_protection);
            return true;
        }
        return false;
    }
    
    
    private long _l;
    private boolean _paused;
    
    @Override
    public void pause(){
        
        _l = timer.getCurrentTick();
        _paused = true;
        
    }
    
    @Override
    public void resume(){
        _paused = false;
        timer.setCurrentTick(_l);
    }
    
}
