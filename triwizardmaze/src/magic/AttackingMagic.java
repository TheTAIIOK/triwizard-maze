package magic;

import maze.Wizard;
import com.golden.gamedev.util.ImageUtil;
import maze.World;
import navigation.Direction;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author godric
 */
public abstract class AttackingMagic extends Magic{

    Direction _direct;
    private int _dam;
    
    public AttackingMagic(World world, Direction d, Wizard mk, int dam) {
        super(world,mk);
        _dam = dam;
        _direct = d;
    }

    public int getDamage() {
        return this._dam;
    }
    
    @Override
    public boolean castASpell() {
        if(super.isCapable()){
            this.setImage(ImageUtil.rotate(this.getImage(), Direction.north().clockwise_angle(_direct)));
            this.setMovement(0.2, (450-_direct.get_angle())%360);
            return true;
        }
        return false;
        
    }
    
}
