/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package magic;

import maze.Wizard;
import maze.World;
import navigation.Direction;

/**
 *
 * @author godric
 */
public final class Perricullum extends Magic{

    private static String IMG_PATH = "resources/pix/magic/periculum.png";

    public Perricullum(World world, Direction d, Wizard mk) {
        super(world, mk);
        setImage(IMG_PATH);
    }

    @Override
    public boolean castASpell() {
        this._maker.setGiveUp(true);
        return true;
    }
    
}
