package magic;

import maze.Wizard;
import maze.World;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author godric
 */
public abstract class DefendingMagic extends Magic {

    protected int _protection;
    public DefendingMagic(World world, Wizard mk, int prt) {
        super(world, mk);
        _protection = prt;
    }
    
}
