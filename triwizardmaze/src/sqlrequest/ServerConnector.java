/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlrequest;

import game.PlayerStatistic;
import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author godric
 */
public class ServerConnector {

    private static int SERVERPORT = 1488;
    private static String ADDRESS = "193.124.188.240";

    public static int canLogin(String login) {
        int result = 0;
        try {
            // Create socket
            SocketAddress sockaddr = new InetSocketAddress(ADDRESS, SERVERPORT);
            // Create an unbound socket
            Socket socket = new Socket();
            // Set timeout while attempting to connect
            socket.connect(sockaddr, 5000);
            // Set timeout for future writes/reads
            socket.setSoTimeout(5000);

            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            DataInputStream in = new DataInputStream(sin);
            DataOutputStream out = new DataOutputStream(sout);

            out.writeInt(RequestServer.RQ_AUTH);
            out.flush();
            out.writeUTF(login);
            out.flush();

            result = in.readInt();
            if (result != RequestServer.ANS_USERCREATE
                    && result != RequestServer.ANS_USEREXIST) {
                result = 0;
            }
            in.close();
            out.close();
            sin.close();
            sout.close();
            socket.close();
        } catch (UnknownHostException ex) { // unknown host 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | NullPointerException ex) { // io exception, service probably not running 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public static void exit() {
        try {
            // Create socket
            SocketAddress sockaddr = new InetSocketAddress(ADDRESS, SERVERPORT);
            // Create an unbound socket
            Socket socket = new Socket();
            // Set timeout while attempting to connect
            socket.connect(sockaddr, 5000);
            // Set timeout for future writes/reads
            socket.setSoTimeout(5000);

            OutputStream sout = socket.getOutputStream();

            DataOutputStream out = new DataOutputStream(sout);

            out.writeInt(RequestServer.RQ_CLOSE);
            out.flush();

            out.close();
            sout.close();
        } catch (Exception ex) {
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static boolean incGoblet(PlayerStatistic pl) {
        boolean res = false;
        try {
            // Create socket
            SocketAddress sockaddr = new InetSocketAddress(ADDRESS, SERVERPORT);
            // Create an unbound socket
            Socket socket = new Socket();
            // Set timeout while attempting to connect
            socket.connect(sockaddr, 5000);
            // Set timeout for future writes/reads
            socket.setSoTimeout(5000);

            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            DataInputStream in = new DataInputStream(sin);
            DataOutputStream out = new DataOutputStream(sout);

            out.writeInt(RequestServer.RQ_INC_GOBLET);
            out.flush();
            out.writeUTF(pl.getPlayerName());
            out.flush();

            int tmp = in.readInt();
            if (tmp == RequestServer.ANS_OK) {
                res = true;
            }

            in.close();
            out.close();
            sin.close();
            sout.close();
            socket.close();
        } catch (Exception ex) {
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    public static boolean updatePlayer(PlayerStatistic player) {
        // TODO update player
        
        boolean result = false;
        try {
            // Create socket
            SocketAddress sockaddr = new InetSocketAddress(ADDRESS, SERVERPORT);
            // Create an unbound socket
            Socket socket = new Socket();
            // Set timeout while attempting to connect
            socket.connect(sockaddr, 5000);
            // Set timeout for future writes/reads
            socket.setSoTimeout(5000);

            OutputStream sout = socket.getOutputStream();

            DataOutputStream out = new DataOutputStream(sout);

            out.writeInt(RequestServer.RQ_UPDATE_STAT);
            out.flush();
            //------------------------------------------------------------------------------
            out.writeUTF(player.getPlayerName());
            out.flush();
            //------------------------------------------------------------------------------
            String shs = "";
            int[] ihs = player.getHighscores();
            for (int i = 0; i < ihs.length; i++) {
                shs += Integer.toString(ihs[i]) + ":";
            }
            out.writeUTF(shs);
            out.flush();
            //------------------------------------------------------------------------------
            out.writeInt(player.getKillCount(true));
            out.flush();
            //------------------------------------------------------------------------------
            out.writeInt(player.getKillCount(false));
            out.flush();
            //------------------------------------------------------------------------------
            out.writeInt(player.getWonGamesCount());
            out.flush();
            //------------------------------------------------------------------------------
            ihs = player.getWonAsCount();
            for (int i = 0; i < ihs.length; i++) {
                out.writeInt(ihs[i]);
                out.flush();    
            }
            //------------------------------------------------------------------------------
            
            
            out.close();
            sout.close();
            socket.close();
            result = true;
        } catch (UnknownHostException ex) { // unknown host 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | NullPointerException ex) { // io exception, service probably not running 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;

    }

    
    public static PlayerStatistic getMyStatistic(String login) {
        PlayerStatistic res = new PlayerStatistic(login);
        
        try {
            // Create socket
            SocketAddress sockaddr = new InetSocketAddress(ADDRESS, SERVERPORT);
            // Create an unbound socket
            Socket socket = new Socket();
            // Set timeout while attempting to connect
            socket.connect(sockaddr, 5000);
            // Set timeout for future writes/reads
            socket.setSoTimeout(5000);

            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            DataInputStream in = new DataInputStream(sin);
            DataOutputStream out = new DataOutputStream(sout);
            
            out.writeInt(RequestServer.RQ_GET_STAT);
            out.flush();
            //------------------------------------------------------------------------------
            out.writeUTF(login);
            out.flush();
            //------------------------------------------------------------------------------
            String[] score = in.readUTF().split(":");
            int[] tmp = new int[10];
            Arrays.fill(tmp, 0);
            for (int i = 0; i < score.length; i++) {
                if (!score[i].isEmpty()) {
                    tmp[i] = Integer.parseInt(score[i]);
                }
            }
            res.setHighscores(tmp);
            //------------------------------------------------------------------------------
            res.setWizardKillCount(in.readInt());
            //------------------------------------------------------------------------------
            res.setMonsterKillCount(in.readInt());
            //------------------------------------------------------------------------------
            res.setGamesWonCount(in.readInt());
            //------------------------------------------------------------------------------
            int[] avas = new int[4];
            for (int i = 0; i < 4; i++) {
                avas[i] = in.readInt();
            }
            res.setWonAsCount(avas);
            //------------------------------------------------------------------------------
            
            in.close();
            out.close();
            sin.close();
            sout.close();
            socket.close();
            
        } catch (UnknownHostException ex) { // unknown host 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
            res = null;
        } catch (IOException | NullPointerException ex) { // io exception, service probably not running 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
            res = null;
        }  catch (Exception ex) {
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        }  
        
        return res;
    }
    
    public static ArrayList<PlayerStatistic> getTopPlayers() {

        ArrayList<PlayerStatistic> res = new ArrayList<PlayerStatistic>();
        try {
            // Create socket
            SocketAddress sockaddr = new InetSocketAddress(ADDRESS, SERVERPORT);
            // Create an unbound socket
            Socket socket = new Socket();
            // Set timeout while attempting to connect
            socket.connect(sockaddr, 5000);
            // Set timeout for future writes/reads
            socket.setSoTimeout(5000);

            if (!socket.isConnected()) {
                return null;
            }

            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            DataInputStream in = new DataInputStream(sin);
            DataOutputStream out = new DataOutputStream(sout);

            out.writeInt(RequestServer.RQ_TOP_PLAYERS);
            out.flush();

            int count = in.readInt();
            String name;
            int cup;
            for (int i = 0; i < count; ++i) {
                name = in.readUTF();
                cup = in.readInt();
                res.add(new PlayerStatistic(name));
            }

            in.close();
            out.close();
            sin.close();
            sout.close();
            socket.close();
        } catch (Exception ex) {
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    public boolean sendSaveFile(String login, File file) {
        
        boolean result = false;
        try {
            // Create socket
            SocketAddress sockaddr = new InetSocketAddress(ADDRESS, SERVERPORT);
            // Create an unbound socket
            Socket socket = new Socket();
            // Set timeout while attempting to connect
            socket.connect(sockaddr, 5000);
            // Set timeout for future writes/reads
            socket.setSoTimeout(5000);

            OutputStream sout = socket.getOutputStream();

            DataOutputStream out = new DataOutputStream(sout);

            out.writeInt(RequestServer.RQ_SAVE_GAME);
            out.flush();
            //------------------------------------------------------------------------------
            out.writeUTF(login);
            out.flush();
            //------------------------------------------------------------------------------
            BasicFileAttributes attr;
            attr = Files.readAttributes(Paths.get(file.getAbsolutePath()), BasicFileAttributes.class);
            out.writeInt((int)attr.size());
            out.flush();
            //------------------------------------------------------------------------------
            
            DataOutputStream dos = new DataOutputStream(sout);
            FileInputStream fis = new FileInputStream(file);
            
            byte[] buffer = new byte[4096];
            while (fis.read(buffer) > 0) {
                dos.write(buffer);
            }

            fis.close();
            dos.close();

            
            //------------------------------------------------------------------------------
            out.close();
            sout.close();
            socket.close();            
            result = true;
        } catch (UnknownHostException ex) { // unknown host 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | NullPointerException ex) { // io exception, service probably not running 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
        
    }

    public File loadSaveFile(String login) {
        
        boolean result = false;
        try {
            // Create socket
            SocketAddress sockaddr = new InetSocketAddress(ADDRESS, SERVERPORT);
            // Create an unbound socket
            Socket socket = new Socket();
            // Set timeout while attempting to connect
            socket.connect(sockaddr, 5000);
            // Set timeout for future writes/reads
            socket.setSoTimeout(5000);

            InputStream sin = socket.getInputStream();
            OutputStream sout = socket.getOutputStream();

            DataInputStream in = new DataInputStream(sin);
            DataOutputStream out = new DataOutputStream(sout);

            out.writeInt(RequestServer.RQ_SAVE_GAME);
            out.flush();
            //------------------------------------------------------------------------------
            out.writeUTF(login);
            out.flush();
            //------------------------------------------------------------------------------
            int filesize = in.readInt();
            //------------------------------------------------------------------------------
            DataInputStream dis = new DataInputStream(sin);
            File file = File.createTempFile("gamesave", ".cer");
            FileOutputStream fos = new FileOutputStream(file);
            byte[] buffer = new byte[4096];

            int read = 0;
            int totalRead = 0;
            int remaining = filesize;
            while ((read = dis.read(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
                totalRead += read;
                remaining -= read;
                fos.write(buffer, 0, read);
            }

            fos.close();

            //------------------------------------------------------------------------------
            in.close();
            out.close();
            sin.close();
            sout.close();
            socket.close();            
            result = true;
        } catch (UnknownHostException ex) { // unknown host 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | NullPointerException ex) { // io exception, service probably not running 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public boolean sendDeleteSave(String login) {
        boolean result = false;
        try {
            // Create socket
            SocketAddress sockaddr = new InetSocketAddress(ADDRESS, SERVERPORT);
            // Create an unbound socket
            Socket socket = new Socket();
            // Set timeout while attempting to connect
            socket.connect(sockaddr, 5000);
            // Set timeout for future writes/reads
            socket.setSoTimeout(5000);

            OutputStream sout = socket.getOutputStream();

            DataOutputStream out = new DataOutputStream(sout);
            //------------------------------------------------------------------------------
            out.writeInt(RequestServer.RQ_DELETE_GAME);
            out.flush();
            //------------------------------------------------------------------------------
            
            out.close();
            sout.close();
            socket.close();
            result = true;
        } catch (UnknownHostException ex) { // unknown host 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | NullPointerException ex) { // io exception, service probably not running 
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ServerConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

}
