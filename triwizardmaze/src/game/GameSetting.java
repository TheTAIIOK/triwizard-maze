/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package game;
import com.golden.gamedev.object.background.ImageBackground;

/**
 * A class of game settings.
 * @author godric
 */
public class GameSetting {

    
    public static ImageBackground _imgbg;
    
    public static enum Heroes {
        HARRY(0), CEDRIC(1), VICTOR(2), FLEUR(3);
        public final int value;
        
        public static Heroes valueOf (int value){
            switch (value){
                case 0:
                    return HARRY;
                case 1:
                    return CEDRIC;
                case 2:
                    return VICTOR;
                case 3:
                    return FLEUR;
                default:
                    return HARRY;
            }
        }
        
        Heroes(int index){
            this.value = index;
        }};
    
    public static int WIDTH = 20;    //labirint width in cells
    public static int HEIGHT = 15;   //labirint height in cells
    public static int FPS = 30;
    public static int TRAP_COUNT = 5;
    public static int FAKE_GOBLET_COUNT = 3;
    public static int SIMPLE_ENEMY_COUNT = 3;
    public static int INVINCIBLE_ENEMY_COUNT = 2;
    
    //----------------------------------------------------
    //--- cnst for game screen
    public static int MAZE_X_LEFT = 40;
    public final static int MAZE_Y_TOP = 50;
    
    public static int MAZE_WIDTH = 800;
    public static int MAZE_HEIGHT = 600;
    public final static int WALL_THIN = 5;
    public static int CHAR_WIDTH = 0;
    public static int CHAR_HEIGHT = 0;
    
    /**
     * Count of static walls in the maze
     */
    public static int STATIC_WALLS = 60;
}
