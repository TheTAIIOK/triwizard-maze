/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameLoader;
import com.golden.gamedev.GameObject;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import view.ChooseActor;
import view.Game;
import view.Login;
import view.MainMenu;
import view.SProfileScene;

/**
 *
 * @author godric
 */
public class Triwizardmaze extends GameEngine {

    { distribute = true; }
    final public static Dimension WINDOW_SIZE = new Dimension(1024, 768);
    private static PlayerStatistic playerInstance = null;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Code application logic here.
        GameLoader game = new GameLoader();
        game.setup(new Triwizardmaze(), WINDOW_SIZE, false);
        game.start();
    }
    
    @Override
    public void initResources() {
        this.setFPS(GameSetting.FPS);
        this.bsInput.setMouseVisible(true);
        
        //---------------------- cache images ----------------------------------
        int i;
        // ChooseActor
        this.bsLoader.storeImage("pix/background/choose.png", getImage("resources/pix/background/choose.png"));
        this.bsLoader.storeImage("pix/player/harry-full.png", getImage("resources/pix/player/harry-full.png"));
        this.bsLoader.storeImage("pix/player/victor-full.png", getImage("resources/pix/player/victor-full.png"));
        this.bsLoader.storeImage("pix/player/fleur-full.png", getImage("resources/pix/player/fleur-full.png"));
        this.bsLoader.storeImage("pix/player/cedric-full.png", getImage("resources/pix/player/cedric-full.png"));

        // Game
        this.bsLoader.storeImages("pix/player/potter", getImages("resources/pix/player/potter.png",4,1));
        this.bsLoader.storeImages("pix/player/delacour", getImages("resources/pix/player/delacour.png",4,1));
        this.bsLoader.storeImages("pix/player/diggory", getImages("resources/pix/player/diggory.png",4,1));
        this.bsLoader.storeImages("pix/player/krum", getImages("resources/pix/player/krum.png",4,1));
        this.bsLoader.storeImage("pix/background/maze.png", getImage("resources/pix/background/maze.png"));
        this.bsLoader.storeImage("pix/hud/hpbar.png", getImage("resources/pix/hud/hpbar.png"));
        this.bsLoader.storeImage("pix/hud/mpbar.png", getImage("resources/pix/hud/mpbar.png"));
        this.bsLoader.storeImages("anim/hp_dragon", getImages("resources/anim/hp_dragon.png", 8 ,1));
        this.bsLoader.storeImages("anim/mp_dragon", getImages("resources/anim/mp_dragon.png", 8 ,1));
        this.bsLoader.storeImage("pix/hud/wandbar", getImage("resources/pix/hud/wandbar.png"));
        this.bsLoader.storeImage("pix/cup.png", getImage("resources/pix/cup.png"));
        this.bsLoader.storeImage("pix/trap.png", getImage("resources/pix/trap.png"));
        this.bsLoader.storeImage("pix/player/harry.png", getImage("resources/pix/player/harry.png"));
        this.bsLoader.storeImage("pix/player/victor.png", getImage("resources/pix/player/victor.png"));
        this.bsLoader.storeImage("pix/player/fleur.png", getImage("resources/pix/player/fleur.png"));
        this.bsLoader.storeImage("pix/player/cedric.png", getImage("resources/pix/player/cedric.png"));
        this.bsLoader.storeImage("pix/hud/diffindo", getImage("resources/pix/magic/diffindo_icon.png"));
        this.bsLoader.storeImage("pix/hud/protego", getImage("resources/pix/magic/protego_icon.png"));
        this.bsLoader.storeImage("pix/hud/expilliarmus", getImage("resources/pix/magic/expilliarmus_icon.png"));
        this.bsLoader.storeImage("pix/hud/ferula", getImage("resources/pix/magic/ferula_icon.png"));
        this.bsLoader.storeImage("pix/hud/apparition", getImage("resources/pix/magic/apparition_icon.png"));
        this.bsLoader.storeImage("pix/wall_west", getImage("resources/pix/wall_west.png"));
        this.bsLoader.storeImage("pix/wall_east", getImage("resources/pix/wall_east.png"));
        this.bsLoader.storeImage("pix/wall_north", getImage("resources/pix/wall_north.png"));
        this.bsLoader.storeImage("pix/wall_south", getImage("resources/pix/wall_south.png"));
        
        this.bsLoader.storeImage("pix/enemy1.png", getImage("resources/pix/enemy1.png"));
        this.bsLoader.storeImage("pix/enemy2.png", getImage("resources/pix/enemy2.png"));
        
        //Magic
        this.bsLoader.storeImage("pix/magic/diffindo", getImage("resources/pix/magic/diffindo.png"));
        this.bsLoader.storeImage("pix/magic/protego", getImage("resources/pix/magic/protego.png"));
        this.bsLoader.storeImage("pix/magic/expilliarmus", getImage("resources/pix/magic/expilliarmus.png"));
        this.bsLoader.storeImage("pix/magic/ferula", getImage("resources/pix/magic/ferula.png"));
        this.bsLoader.storeImage("pix/magic/periculum", getImage("resources/pix/magic/periculum.png"));
        
        //Endgame Screen
        this.bsLoader.storeImage("pix/endscreen/end_angel", getImage("resources/pix/endscreen/end_angel.png"));
        this.bsLoader.storeImage("pix/endscreen/header", getImage("resources/pix/endscreen/header.png"));
        this.bsLoader.storeImage("pix/endscreen/msg_die", getImage("resources/pix/endscreen/msg_die.png"));
        this.bsLoader.storeImage("pix/endscreen/msg_gup", getImage("resources/pix/endscreen/msg_gup.png"));
        this.bsLoader.storeImage("pix/endscreen/msg_win", getImage("resources/pix/endscreen/msg_win.png"));
        this.bsLoader.storeImage("pix/endscreen/win_cedric", getImage("resources/pix/endscreen/win_cedric.png"));
        this.bsLoader.storeImage("pix/endscreen/win_cedric_label", getImage("resources/pix/endscreen/win_cedric_label.png"));
        this.bsLoader.storeImage("pix/endscreen/win_potter", getImage("resources/pix/endscreen/win_potter.png"));
        this.bsLoader.storeImage("pix/endscreen/win_potter_label", getImage("resources/pix/endscreen/win_potter_label.png"));
        this.bsLoader.storeImage("pix/endscreen/win_fleur", getImage("resources/pix/endscreen/win_fleur.png"));
        this.bsLoader.storeImage("pix/endscreen/win_fleur_label", getImage("resources/pix/endscreen/win_fleur_label.png"));
        this.bsLoader.storeImage("pix/endscreen/win_krum_label", getImage("resources/pix/endscreen/win_krum_label.png"));
        this.bsLoader.storeImage("pix/endscreen/win_krum", getImage("resources/pix/endscreen/win_krum.png"));
        
        // Login
        this.bsLoader.storeImage("pix/background/menu.png", getImage("resources/pix/background/menu.png"));
        
        // MainMenu
        
        BufferedImage[] _flameRaw = new BufferedImage[32];
        for (i = 0; i < 32; ++i) {
            _flameRaw[i] = getImage("resources/anim/flame_"
                    + ((i < 10)
                            ? ("0" + i)
                            : i)
                    + ".png");
        }
        this.bsLoader.storeImages("anim/flame_", _flameRaw);     
    }

    public static PlayerStatistic getPlayerInstance() {
        if (playerInstance == null) {
            playerInstance = new PlayerStatistic("Lord Voldemort");
        }
        return playerInstance;
    }

    public static PlayerStatistic setPlayerInstance(PlayerStatistic instance) {
        if (playerInstance == null) {
            playerInstance = instance;
        }
        return playerInstance;
    }

    @Override
    public GameObject getGame(int GameId) {

        try {
        switch (GameId) {
            case 0: // login screen
                return new Login(this);
            case 1:
                return new MainMenu(this, false);
            case 2:
                return new ChooseActor(this);
            case 3:
                return new Game(this, game.GameSetting.Heroes.HARRY);
            case 4:
                return new Game(this, game.GameSetting.Heroes.VICTOR);
            case 5:
                return new Game(this, game.GameSetting.Heroes.FLEUR);
            case 6:
                return new Game(this, game.GameSetting.Heroes.CEDRIC);
            case 9:
                return new SProfileScene(this);
            case 10:
                return new Game(this);
        }
        } catch (IOException ex ) {
            boolean test = ex.getClass().equals(Game.class);
            
            return new MainMenu(this, true);
        } catch (ClassNotFoundException ex) {
            return new MainMenu(this, true);
        }

        return null;
    }

}
