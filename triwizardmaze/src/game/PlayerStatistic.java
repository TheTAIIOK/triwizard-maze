/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 *
 * @author godric
 */
public class PlayerStatistic implements Externalizable {

    /**
     * Player's nickname
     */
    private String _nickname;

    /**
     * Player's 10 best highscores
     */
    private int _highscores[];

    /**
     * Player's count of killed wizards
     */
    private int _wizardKillCount;

    /**
     * Player's count of monsters
     */
    private int _monsterKillCount;

    /**
     * Player's count of won games
     */
    private int _gamesWonCount;

    /**
     * Player's wins as each of four avatars
     */
    private int _WonAsCount[];

    /**
     * Player's highscore in game session. Must be nulled on start and saved on exit
     */
    public int lastHighscore;
    
    /**
     * Player's achievement's statistic (Not implemented)
     */
    //private Achievements;
    public PlayerStatistic() {
        _nickname = "";
        _highscores = new int[10];
        _wizardKillCount = 0;
        _monsterKillCount = 0;
        _gamesWonCount = 0;
        _WonAsCount = new int[4];
        lastHighscore = 0;
    }

    /**
     * Creates player profile, which store his statistic
     *
     * @param nick player's selected login
     */
    public PlayerStatistic(String nick) {
        this();
        _nickname = nick;
    }

    /**
     * Getting player' nickname (login)
     *
     * @return player' nickname (login)
     */
    public String getPlayerName() {
        return this._nickname;
    }

    /**
     * Getting player's kill count.
     *
     * @param isWizard if flag set, returns wizard's kill count, otherwise -
     * monster
     * @return player's stored kill count of selected creatures
     */
    public int getKillCount(boolean isWizard) {
        return isWizard ? this._wizardKillCount : this._monsterKillCount;
    }

    /**
     * Getting all player's kills count
     *
     * @return summary kills of player
     */
    public int getSummaryKillCount() {
        return this._wizardKillCount + this._monsterKillCount;
    }

    public int[] getWonAsCount() {
        return _WonAsCount;
    }

    public void setNickname(String _nickname) {
        this._nickname = _nickname;
    }

    public void setHighscores(int[] _highscores) {
        this._highscores = _highscores;
    }

    public void setWizardKillCount(int _wizardKillCount) {
        this._wizardKillCount = _wizardKillCount;
    }

    public void setMonsterKillCount(int _monsterKillCount) {
        this._monsterKillCount = _monsterKillCount;
    }

    public void setGamesWonCount(int _gamesWonCount) {
        this._gamesWonCount = _gamesWonCount;
    }

    public void setWonAsCount(int[] _WonAsCount) {
        this._WonAsCount = _WonAsCount;
    }
    
    /**
     * Adding one kill to player
     *
     * @param isWizard if flag set, kill counts as wizard kill, otherwise - as
     * monster kill
     */
    public void addKill(boolean isWizard) {
        if (isWizard) {
            this._wizardKillCount++;
        } else {
            this._monsterKillCount++;
        }
    }

    /**
     * Getting won games count
     *
     * @return won games count
     */
    public int getWonGamesCount() {
        return this._gamesWonCount;
    }

    /**
     * Increment won games count and adding win to selected player's avatar
     *
     * @param playersIndex index of selected player's avatar
     */
    public void addWonGame(int playersIndex) {
        ++_gamesWonCount;
        ++_WonAsCount[playersIndex];
    }

    /**
     * Adding new highscore to player statistic
     *
     * @param value - new highscore
     */
    public void addHighscore(int value) { 
        for (int insertIndex = 0; insertIndex < _highscores.length; ++insertIndex){
            if (_highscores[insertIndex] >= value){
                continue;
            } else {
                for (int j = _highscores.length - 1; j > insertIndex; --j ){
                    _highscores[j] = _highscores[j - 1];
                }
                _highscores[insertIndex] = value;
                break;
            }            
        }
    }

    /**
     * Getting player's highscores list
     *
     * @return player's highscores, sorted by ascending
     */
    public int[] getHighscores() {
        return _highscores.clone();
    }

    /**
     * Overriden serializing function
     * @param out output thread
     * @throws IOException output error
     */
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF(_nickname);

        int notNulledHighscores = 0;

        for (int highscore : _highscores) {
            if (highscore != 0) {
                notNulledHighscores++;
            }
        }

        out.writeInt(lastHighscore);
        
        out.writeInt(notNulledHighscores);

        for (int i = 0; i < notNulledHighscores; ++i) {
            out.writeInt(_highscores[i]);
        }

        out.writeInt(_wizardKillCount);
        out.writeInt(_monsterKillCount);
        out.writeInt(_gamesWonCount);

        for (int avatarWon : _WonAsCount) {
            out.writeInt(avatarWon);
        }
    }

    /**
     * Overriden deserializing function
     * @param in input thread
     * @throws IOException error input-outout thread 
     * @throws ClassNotFoundException error if incorrect class
     */
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        _nickname = in.readUTF();

        lastHighscore = in.readInt();
        
        int notNulledHighscores = in.readInt();

        for (int i = 0; i < notNulledHighscores; ++i) {
            _highscores[i] = in.readInt();
        }

        _wizardKillCount = in.readInt();
        _monsterKillCount = in.readInt();
        _gamesWonCount = in.readInt();

        for (int i = 0; i < 4; ++i) {
            _WonAsCount[i] = in.readInt();
        }
    }
}
