/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triwizardmaze.themegtge;

import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.theme.basic.BButtonRenderer;
import com.golden.gamedev.gui.toolkit.GraphicsUtil;
import com.golden.gamedev.gui.toolkit.TComponent;
import com.golden.gamedev.gui.toolkit.UIConstants;
import com.golden.gamedev.object.GameFont;
import com.golden.gamedev.object.font.SystemFont;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import view.gui.TImageButton;

/**
 *
 * @author Alexander Lyashenko
 */
public class TMImageButtonRenderer extends BButtonRenderer {

    private int scaleOffset;
    
    public TMImageButtonRenderer() {
        this.put("Background Over Color", new Color(255, 255, 255, 20));
        this.put("Background Disabled Color", new Color(0, 0, 0, 100));
    }

    @Override
    public String UIName() {
        return "Image Button";
    }

    @Override
    public String[] UIDescription() {
        return new String[]{
            "Button", "Button Over", "Button Pressed", "Button Disabled"
        };
    }

    @Override
    public BufferedImage[] createUI(TComponent component, int w, int h) {
        
        BufferedImage raw = ((TImageButton) component).getButtonImage();
        int scaledWidth = (int)((double)raw.getWidth() * 0.9);
        int scaledHeight = (int)((double)raw.getHeight() * 0.9);
        scaleOffset = (raw.getWidth() - scaledWidth)/2;
        
        BufferedImage scaledDarkRaw = 
                new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
        BufferedImage scaledWhiteRaw = 
                new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_ARGB);
        
        Graphics2D painter = scaledDarkRaw.createGraphics();
        painter.drawImage(raw, 0, 0, scaledWidth, scaledWidth, null);
        painter.setColor((Color)this.get("Background Disabled Color", component));
        painter.fillOval(0, 0, scaledWidth, scaledHeight);
        
        painter = scaledWhiteRaw.createGraphics();
        painter.drawImage(raw, 0, 0, scaledWidth, scaledWidth, null);
        painter.setColor((Color)this.get("Background Over Color", component));
        painter.fillOval(0, 0, scaledWidth, scaledHeight);
        
        BufferedImage[] ui = new BufferedImage[]{
            raw,
            scaledWhiteRaw,
            scaledDarkRaw
        };

        return ui;
    }

    @Override
    public void processUI(TComponent component, BufferedImage[] ui) {
//		TButton button = (TButton) component;
//		
//		String[] color = new String[] {
//		        "Text Color", "Text Over Color", "Text Pressed Color",
//		        "Text Disabled Color"
//		};
//		String[] font = new String[] {
//		        "Text Font", "Text Over Font", "Text Pressed Font",
//		        "Text Disabled Font"
//		};
//		
//		String[] document = GraphicsUtil.parseString(button.getText());
//		for (int i = 0; i < 4; i++) {
//			Graphics2D g = ui[i].createGraphics();
//			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
//			        RenderingHints.VALUE_ANTIALIAS_ON);
//			GraphicsUtil.drawString(g, document, button.getWidth(), button
//			        .getHeight(), (GameFont) this.get(font[i], component),
//			        (Color) this.get(color[i], component), (Integer) this.get(
//			                "Text Horizontal Alignment Integer", component),
//			        (Integer) this.get("Text Vertical Alignment Integer",
//			                component), (Insets) this.get("Text Insets",
//			                component), (Integer) this.get(
//			                "Text Vertical Space Integer", component));
//			g.dispose();
//		}
    }

    public void renderUI(Graphics2D g, int x, int y, TComponent component, BufferedImage[] ui) {
        TButton button = (TButton) component;

        if (!button.isEnabled()) {
            g.drawImage(ui[2], x + scaleOffset, y + scaleOffset, null);
        } else if (button.isMousePressed() || button.isMouseOver()) {
            g.drawImage(ui[1], x + scaleOffset, y + scaleOffset, null);
        } else {
            g.drawImage(ui[0], x, y, null);
        }
    }

}
