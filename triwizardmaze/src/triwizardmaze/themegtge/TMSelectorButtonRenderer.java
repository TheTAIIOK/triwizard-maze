/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triwizardmaze.themegtge;

import com.golden.gamedev.gui.theme.basic.BButtonRenderer;
import com.golden.gamedev.gui.toolkit.GraphicsUtil;
import com.golden.gamedev.gui.toolkit.TComponent;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Transparency;
import java.awt.image.BufferedImage;

/**
 *
 * @author Alexander Lyashenko
 */
public class TMSelectorButtonRenderer extends BButtonRenderer {

    public TMSelectorButtonRenderer() {
        super();
        this.put("Background Color", new Color(220,220,220,20));
        this.put("Background Over Color", new Color(0,0,0,0));
        this.put("Background Pressed Color", new Color(0,0,0,0));
        this.put("Background Border Color", Color.ORANGE);
        this.put("Background Disabled Color", new Color(0,0,0,0));
    }

    @Override
    public BufferedImage[] createUI(TComponent component, int w, int h) {
        BufferedImage[] ui = GraphicsUtil.createImage(4, w, h,
                Transparency.TRANSLUCENT);

        String[] color = new String[]{
            "Background Color", "Background Over Color",
            "Background Pressed Color", "Background Disabled Color"
        };

        Color[] borderColor = new Color[]{
            null,
            (Color) this.get("Background Border Color", component),
            null,            
            null};

        for (int i = 0; i < 4; i++) {
            Graphics2D g = ui[i].createGraphics();
            g.setColor((Color) this.get(color[i], component));
            g.fillRect(0, 0, w - 1, h - 1);
            if (borderColor[i] != null) {
                g.setColor(borderColor[i]);
                g.setStroke(new BasicStroke(3));
                g.drawRect(0, 0, w - 1, h - 1);
            }
            g.dispose();
        }

        return ui;
    }
    
    @Override
    public String UIName(){
        return "Selector button";
    }
}
