/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triwizardmaze.themegtge;

import com.golden.gamedev.gui.TButton;
import com.golden.gamedev.gui.toolkit.GraphicsUtil;
import com.golden.gamedev.gui.toolkit.TComponent;
import com.golden.gamedev.gui.toolkit.UIConstants;
import com.golden.gamedev.object.GameFont;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

/**
 *
 * @author still
 */
public class TMMultilineButtonRenderer extends TMButtonRenderer{
    @Override
    public String UIName() {
        return "Multiline button";
    }
    
    @Override
    public void processUI(TComponent component, BufferedImage[] ui) {
        TButton button = (TButton) component;

        String[] font = new String[]{
            "Text Font", "Text Font", "Text Pressed Font",
            "Text Font"
        };

        String[] color = new String[]{"Text Color",
            "Text Over Color", "Text Pressed Color", "Text Disabled Color"};

        String[] document = GraphicsUtil.parseString(button.getText());
        for (int i = 0; i < 4; i++) {
            Graphics2D g = ui[i].createGraphics();
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            drawString(
                    g,
                    document,
                    button.getWidth(),
                    button.getHeight(),
                    (GameFont) this.get(font[i], component),
                    (Color) this.get(color[i], component),
                    (Integer) this.get(
                            "Text Horizontal Alignment Integer", component),
                    (Integer) this.get(
                            "Text Vertical Alignment Integer", component),
                    null,
                    (Integer) this.get(
                            "Text Vertical Space Integer", component));
            g.dispose();
        }
    }
    
    private void drawString(Graphics2D g, String[] document, int w, int h, GameFont font, Color color, Integer hAlignment, Integer vAlignment, Insets inset, Integer vSpace) {
        if (inset == null) {
            inset = new Insets(0, 0, 0, 0);;
        }
        if (hAlignment == null) {
            hAlignment = UIConstants.CENTER;
        }
        if (vAlignment == null) {
            vAlignment = UIConstants.CENTER;
        }
        if (vSpace == null) {
            vSpace = 0;
        }

        int space = vSpace.intValue();
        int height = (document.length * (font.getHeight() + space)) - space;

        int y = 0;
        if (vAlignment == UIConstants.TOP) {
            y = inset.top;
        } else if (vAlignment == UIConstants.BOTTOM) {
            y = h - inset.bottom - height;
        } else if (vAlignment == UIConstants.CENTER) {
            y = (h / 2) - (height / 3);
        }

        g.setColor(color);
        for (int i = 0; i < document.length; i++) {
            font.drawString(g, document[i], hAlignment.intValue(), inset.left,
                    y, w - inset.left - inset.right);

            y += font.getHeight() / 2;
        }
    }
}
