/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triwizardmaze.themegtge;

import com.golden.gamedev.gui.toolkit.TComponent;
import com.golden.gamedev.gui.toolkit.UIRenderer;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import view.gui.TAnimatedComponent;

/**
 *
 * @author still
 */
public class TMAnimatedRenderer extends UIRenderer {

    @Override
    public String UIName() {
        return "Animated Component";
    }

    @Override
    public String[] UIDescription() {
        return new String[] {
		        "Component", "Animated Component"
		};
    }

    @Override
    public BufferedImage[] createUI(TComponent tc, int i, int i1) {
        TAnimatedComponent sprite = (TAnimatedComponent)tc;
        
        BufferedImage[] result = new BufferedImage[1];
        result[0] = new BufferedImage(sprite.getWidth(), sprite.getHeight(), 1);
        //result[1] = new BufferedImage(sprite.getWidth(), sprite.getHeight(), 1);
        System.out.print("Called createUI\n");
        return result;
        
    }

    @Override
    public void processUI(TComponent tc, BufferedImage[] bis) {
    }

    @Override
    public void renderUI(Graphics2D gd, int x, int y, TComponent tc, BufferedImage[] ui) {
        gd.drawImage(ui[0], x, y, null);
    }

}
