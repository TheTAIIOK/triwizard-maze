package com.company;

import java.util.*;


//sql import

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class TaskManager implements Runnable {


    // username and password of MySQL server
    private static final String url = "jdbc:mysql://193.124.188.240:3306/triwizard?useSSL=false";
    private static final String user = "root";
    private static final String password = "Dp05pnmf6A37d4e2";


    // variables for opening and managing connection
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;


    public void setWinner(int ID , String User)
    {

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String sql = "UPDATE OnlineGames " +
                    "SET Winner = "+ User +" WHERE ID='" + ID + "'" ;
            stmt.executeUpdate(sql);

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

    }

    /**
     * Method check mysql connection
     */
    public void checkMSQLConnection() {
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public boolean getOnlineStatus(String User)
    {

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String query = "SELECT Online FROM Users WHERE User='" + User + "'";

            // executing SELECT query

            rs = stmt.executeQuery(query);

            if (rs.next()) {
                boolean status = rs.getBoolean(1);
                return status;
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }

        return false;
    }

    public void checkInviteStatus() {

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String query = "select ID, User1, User2   from Invite";

            rs = stmt.executeQuery(query);

            while (rs.next()) {
                int ID = rs.getInt(1);
                String User1 = rs.getString(2);
                String User2 = rs.getString(3);

                boolean first = getOnlineStatus(User1);
                boolean second = getOnlineStatus(User2);

                if(!first || !second)
                {
                    String sql = "DELETE FROM Invite " +
                            "WHERE id = '"+ ID +"'"  ;
                    stmt.executeUpdate(sql);
                }

            }

            return ;


        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return ;
    }


    public void checkUserOnlineGamesResult() {

        try {

            // getting Statement object to execute query
            stmt = con.createStatement();

            String query = "select ID, User1, Score1, Time1, User2 , Score2, Time2  from OnlineGames";

            rs = stmt.executeQuery(query);

            while (rs.next()) {
                int ID = rs.getInt(1);
                String User1 = rs.getString(2);
                int Score1 = rs.getInt(3);
                String Time1 = rs.getString(4);

                String User2 = rs.getString(5);
                int Score2 = rs.getInt(6);
                String Time2 = rs.getString(7);

                if(Score1 != 0 && !Time1.isEmpty() && Score2 != 0 && !Time2.isEmpty() )
                {
                    if(Score1 > Score2) {
                        setWinner(ID, User1);
                    }
                    else
                        setWinner(ID,User2);
                }
            }

            return ;


        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
        return ;
    }

    /**
     * Run method of new thread
     */
    public void run() {

            checkMSQLConnection();

            Timer timer = new Timer();

            timer.schedule(new TimerTask() {
            @Override
            public void run() {
                checkUserOnlineGamesResult();
                checkInviteStatus();
            }
        }, 4000, 4000);

    }
}




