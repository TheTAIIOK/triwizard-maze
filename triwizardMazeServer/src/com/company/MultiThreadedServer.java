package com.company;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


//sql import

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class MultiThreadedServer implements Runnable {

    protected int serverPort = 1488;
    protected ServerSocket serverSocket = null;
    protected boolean isStopped = false;
    protected Thread runningThread = null;

    public MultiThreadedServer(int port) {
        this.serverPort = port;
    }

    /**
     * Run method of new thread
     */
    public void run() {
        synchronized (this) {
            this.runningThread = Thread.currentThread();
        }
        DataBaseExist();
        openServerSocket();

        new Thread(new TaskManager()).start();

        while (!isStopped()) {
            Socket clientSocket = null;
            try {
                clientSocket = this.serverSocket.accept();
            } catch (IOException e) {
                if (isStopped()) {
                    System.out.println("Server Stopped.");
                    return;
                }
                throw new RuntimeException(
                        "Error accepting client connection", e);
            }
            new Thread(
                    new WorkerRunnable(
                            clientSocket, "Game Server")
            ).start();
        }
        System.out.println("Server Stopped.");
    }

    /**
     * Get status of server
     *
     * @return
     */
    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    /**
     * Stop Server
     */
    public synchronized void stop() {
        this.isStopped = true;
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

    /**
     * Method open server socket or return that port is busy
     */
    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port 8888", e);
        }
    }

    /**
     * Method check if database exist if it doesn't exist create it
     */
    public void DataBaseExist(){

        // username and password of MySQL server
         String url = "jdbc:mysql://193.124.188.240:3306/?useSSL=false";
         String user = "root";
         String password = "Dp05pnmf6A37d4e2";


        // variables for opening and managing connection
         Connection con;
         Statement stmt;
         ResultSet rs;

        try {
            con = DriverManager.getConnection(url, user, password);

            rs = con.getMetaData().getCatalogs();
            String dbname = "triwizard";

            boolean exists = false;
            while(rs.next())
            {
                String catalog = rs.getString(1);
                if(dbname.equals(catalog))
                {
                    exists = true;
                }
            }
            // Close connection, statement, and result set.
           if (!exists)
           {

               stmt = con.createStatement();
               String sql = "CREATE DATABASE triwizard";
               stmt.executeUpdate(sql);

               String Invite = "CREATE TABLE triwizard.Invite " +
                       "(ID INTEGER," +
                       "User1 VARCHAR(255)," +
                       "User2 VARCHAR(255)," +
                       "Status INTEGER )";

               stmt.executeUpdate(Invite);

               String Users = "CREATE TABLE triwizard.Users " +
                       " (User VARCHAR(255), " +
                       " Folder VARCHAR(255)," +
                       " Online BOOL)";

               stmt.executeUpdate(Users);

               String UserStat = "CREATE TABLE triwizard.UserStat " +
                       "(User VARCHAR(255)," +
                       "HighScores VARCHAR(255)," +
                       "WizardKill INTEGER, " +
                       "MonsterKill INTEGER, " +
                       "GamesWon INTEGER, " +
                       "FirstAvatar INTEGER, " +
                       "SecondAvatar INTEGER, " +
                       "ThirdAvatar INTEGER, " +
                       "FourAvatar INTEGER )";
               stmt.executeUpdate(UserStat);

               String OnlineGames = "CREATE TABLE triwizard.OnlineGames " +
                       "(ID INTEGER," +
                       "User1 VARCHAR(255)," +
                       "Score1 INTEGER, " +
                       "Time1 VARCHAR(255)," +
                       "User2 VARCHAR(255)," +
                       "Score2 INTEGER, " +
                       "Time2 VARCHAR(255)," +
                       "Winner VARCHAR(255))";

               stmt.executeUpdate(OnlineGames);

               String OnlineGamesStat = "CREATE TABLE triwizard.OnlineGames " +
                       "(ID INTEGER," +
                       "User VARCHAR(255)," +
                       "HidhScores VARCHAR(255)," +
                       "WizardKill INTEGER, " +
                       "MonsterKill INTEGER, " +
                       "GamesWon INTEGER, " +
                       "FirstAvatar INTEGER, " +
                       "SecondAvatar INTEGER, " +
                       "ThirdAvatar INTEGER, " +
                       "FourAvatar INTEGER )";

               stmt.executeUpdate(OnlineGamesStat);





               stmt.close();

        }
        con.close();
        rs.close();

        }
        catch (SQLException sqlEx){}


    }


}

